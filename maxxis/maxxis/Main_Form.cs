﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace maxxis
{
    public partial class Main_Form : Form
    {
        string con = @"Data Source=LAPTOP-N5RT3GVU\SQLEXPRESS;Initial Catalog=DBMaxxis;Integrated Security=True";
        public Main_Form()
        {
            InitializeComponent();
        }

        private void btn_GuestMessage_Click(object sender, EventArgs e)
        {

        }

        private void btn_GuestFolio_Click(object sender, EventArgs e)
        {
            Guest_Folio GF = new Guest_Folio();
            GF.ShowDialog();
        }

        private void pnl_MainPanel_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btn_GuestArrivalList_Click(object sender, EventArgs e)
        {
            guestArrivalList_form GAF = new guestArrivalList_form();
            GAF.ShowDialog();
        }

        private void btn_New_Walkin_Guest_Click(object sender, EventArgs e)
        {
            WalkinReservation_form WRF = new WalkinReservation_form();
            WRF.ShowDialog();
        }

        private void Main_Form_Load(object sender, EventArgs e)
        {
            //Urban View Rooms
            btn_Room21.Text = "  21 \n UVR (4/0) \n \n P 5,700.00";
            btn_Room31.Text = "  31 \n UVR (4/0) \n \n P 5,700.00";
            btn_Room51.Text = "  51 \n UVR (4/0) \n \n P 5,700.00";
            btn_Room61.Text = "  61 \n UVR (4/0) \n \n P 5,700.00";
            btn_Room71.Text = "  71 \n UVR (4/0) \n \n P 5,700.00";
            btn_Room81.Text = "  81 \n UVR (4/0) \n \n P 5,700.00";
            btn_Room91.Text = "  91 \n UVR (4/0) \n \n P 5,700.00";
            btn_Room101.Text = "  101 \n UVR (4/0) \n \n P 5,700.00";
        
            btn_Room26.Text = "  26 \n UVR (4/0) \n \n P 5,700.00";
            btn_Room36.Text = "  36 \n UVR (4/0) \n \n P 5,700.00";
            btn_Room56.Text = "  56 \n UVR (4/0) \n \n P 5,700.00";
            btn_Room66.Text = "  66 \n UVR (4/0) \n \n P 5,700.00";
            btn_Room76.Text = "  76 \n UVR (4/0) \n \n P 5,700.00";
            btn_Room86.Text = "  86 \n UVR (4/0) \n \n P 5,700.00";
            btn_Room96.Text = "  96 \n UVR (4/0) \n \n P 5,700.00";
            btn_Room106.Text = "  106 \n UVR (4/0) \n \n P 5,700.00";

            //Urban Corner Room
            btn_Room22.Text = "  22 \n UCR (2/0) \n \n P 4,950.00";
            btn_Room32.Text = "  32 \n UCR (2/0) \n \n P 4,950.00";
            btn_Room52.Text = "  52 \n UCR (2/0) \n \n P 4,950.00";
            btn_Room62.Text = "  62 \n UCR (2/0) \n \n P 4,950.00";
            btn_Room72.Text = "  72 \n UCR (2/0) \n \n P 4,950.00";
            btn_Room82.Text = "  82 \n UCR (2/0) \n \n P 4,950.00";
            btn_Room92.Text = "  92 \n UCR (2/0) \n \n P 4,950.00";
            btn_Room102.Text = "  102 \n UCR (2/0) \n \n P 4,950.00";

            btn_Room25.Text = "  25 \n UCR (2/0) \n \n P 4,950.00";
            btn_Room35.Text = "  35 \n UCR (2/0) \n \n P 4,950.00";
            btn_Room55.Text = "  55 \n UCR (2/0) \n \n P 4,950.00";
            btn_Room65.Text = "  65 \n UCR (2/0) \n \n P 4,950.00";
            btn_Room75.Text = "  75 \n UCR (2/0) \n \n P 4,950.00";
            btn_Room85.Text = "  85 \n UCR (2/0) \n \n P 4,950.00";
            btn_Room95.Text = "  95 \n UCR (2/0) \n \n P 4,950.00";
            btn_Room105.Text = "  105 \n UCR (2/0) \n \n P 4,950.00";

            //Urban Room PWD
            btn_Room23.Text = "  23 \n PWD (2/0) \n \n P 4200.00";

            //Urban Room
            btn_Room33.Text = "  33 \n UCR (2/0) \n \n P 4200.00";
            btn_Room53.Text = "  53 \n UCR (2/0) \n \n P 4200.00";
            btn_Room63.Text = "  63 \n UCR (2/0) \n \n P 4200.00";
            btn_Room73.Text = "  73 \n UCR (2/0) \n \n P 4200.00";
            btn_Room83.Text = "  83 \n UCR (2/0) \n \n P 4200.00";
            btn_Room93.Text = "  93 \n UCR (2/0) \n \n P 4200.00";
            btn_Room103.Text = "  103 \n UCR (2/0) \n \n P 4200.00";

            btn_Room24.Text = "  24 \n UCR (2/0) \n \n P 4200.00";
            btn_Room34.Text = "  34 \n UCR (2/0) \n \n P 4200.00";
            btn_Room54.Text = "  54 \n UCR (2/0) \n \n P 4200.00";
            btn_Room64.Text = "  64 \n UCR (2/0) \n \n P 4200.00";
            btn_Room74.Text = "  74 \n UCR (2/0) \n \n P 4200.00";
            btn_Room84.Text = "  84 \n UCR (2/0) \n \n P 4200.00";
            btn_Room94.Text = "  94 \n UCR (2/0) \n \n P 4200.00";
            btn_Room104.Text = "  104 \n UCR (2/0) \n \n P 4200.00";

            //Methods
        }

        private void btn_SignOut_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_Room21_Click(object sender, EventArgs e)
        {

        }

        private void btn_reservationList_Click(object sender, EventArgs e)
        {
            ReservationList_Form RF = new ReservationList_Form();
            RF.ShowDialog();
        }
    }
}
