﻿namespace maxxis
{
    partial class Guest_Folio
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
<<<<<<< HEAD
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
=======
<<<<<<< HEAD
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
=======
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
>>>>>>> 3931348c34a4e1b1de18778096fd19dc0e91573a
>>>>>>> 522a8075ec8125153832dce14daa61967b8a869e
            this.guestFolio_panel = new System.Windows.Forms.Panel();
            this.lbl_guestFolio = new System.Windows.Forms.Label();
            this.pcb_guestFolio = new System.Windows.Forms.PictureBox();
            this.pnl_guestFolio = new System.Windows.Forms.Panel();
            this.btn_checkOut = new System.Windows.Forms.Button();
            this.btn_GoBack = new System.Windows.Forms.Button();
            this.tabControl2 = new System.Windows.Forms.TabControl();
            this.tab_guestRemarks = new System.Windows.Forms.TabPage();
            this.pnl_guestRemarks = new System.Windows.Forms.Panel();
            this.txt_guestRemarks = new System.Windows.Forms.TextBox();
            this.tab_billingSummary = new System.Windows.Forms.TabPage();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tab_guestInformation = new System.Windows.Forms.TabPage();
            this.cmb_homeCountry = new System.Windows.Forms.ComboBox();
            this.lbl_homeCountry = new System.Windows.Forms.Label();
            this.lbl_companyAgency = new System.Windows.Forms.Label();
            this.txt_companyAgency = new System.Windows.Forms.TextBox();
            this.lbl_dateOfBirth = new System.Windows.Forms.Label();
            this.dtb_dateOfBirth = new System.Windows.Forms.DateTimePicker();
            this.lbl_province = new System.Windows.Forms.Label();
            this.txt_province = new System.Windows.Forms.TextBox();
            this.lbl_city = new System.Windows.Forms.Label();
            this.txt_city = new System.Windows.Forms.TextBox();
            this.lbl_address = new System.Windows.Forms.Label();
            this.txt_address = new System.Windows.Forms.TextBox();
            this.txt_middleName = new System.Windows.Forms.TextBox();
            this.lbl_middleName = new System.Windows.Forms.Label();
            this.lbl_title = new System.Windows.Forms.Label();
            this.lbl_email = new System.Windows.Forms.Label();
            this.txt_email = new System.Windows.Forms.TextBox();
            this.lbl_contactDetails = new System.Windows.Forms.Label();
            this.txt_contactDetails = new System.Windows.Forms.TextBox();
            this.lbl_last_name = new System.Windows.Forms.Label();
            this.lbl_showRates = new System.Windows.Forms.Label();
            this.pcb_search = new System.Windows.Forms.PictureBox();
            this.pcb_info = new System.Windows.Forms.PictureBox();
            this.pcb_plusButton = new System.Windows.Forms.PictureBox();
            this.txt_firstname = new System.Windows.Forms.TextBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.lbl_firstname = new System.Windows.Forms.Label();
            this.txt_lastname = new System.Windows.Forms.TextBox();
            this.tab_settlementOptions = new System.Windows.Forms.TabPage();
            this.lbl_modeOfPayment = new System.Windows.Forms.Label();
            this.rbtn_partialCredit = new System.Windows.Forms.RadioButton();
            this.rbtn_credit = new System.Windows.Forms.RadioButton();
            this.rbtn_cash = new System.Windows.Forms.RadioButton();
            this.pnl_stayInformation = new System.Windows.Forms.Panel();
            this.dtp_departureTime = new System.Windows.Forms.DateTimePicker();
            this.dtp_arrivalTime = new System.Windows.Forms.DateTimePicker();
            this.dtp_departureDate = new System.Windows.Forms.DateTimePicker();
            this.dtp_arrivalDate = new System.Windows.Forms.DateTimePicker();
            this.dom_child = new System.Windows.Forms.DomainUpDown();
            this.dom_adults = new System.Windows.Forms.DomainUpDown();
            this.dom_numberOfNights = new System.Windows.Forms.DomainUpDown();
            this.lbl_child = new System.Windows.Forms.Label();
            this.lbl_adults = new System.Windows.Forms.Label();
            this.lbl_numberOfNights = new System.Windows.Forms.Label();
            this.lbl_departure = new System.Windows.Forms.Label();
            this.lbl_arrival = new System.Windows.Forms.Label();
            this.stayInformation_panel = new System.Windows.Forms.Panel();
            this.lbl_stayInformation = new System.Windows.Forms.Label();
            this.txt_folioNumber = new System.Windows.Forms.TextBox();
            this.lbl_folioNumber = new System.Windows.Forms.Label();
            this.txt_billTo = new System.Windows.Forms.TextBox();
            this.lbl_billTo = new System.Windows.Forms.Label();
            this.txt_expire = new System.Windows.Forms.TextBox();
            this.lbl_expire = new System.Windows.Forms.Label();
            this.txt_cardNumber = new System.Windows.Forms.TextBox();
            this.lbl_cardNumber = new System.Windows.Forms.Label();
            this.cmb_type = new System.Windows.Forms.ComboBox();
            this.lbl_type = new System.Windows.Forms.Label();
            this.pcb_reloadGuestFolio = new System.Windows.Forms.PictureBox();
            this.pcb_searchFolioNumber = new System.Windows.Forms.PictureBox();
            this.pcb_addBillTo = new System.Windows.Forms.PictureBox();
            this.pcb_searchBillTo = new System.Windows.Forms.PictureBox();
            this.tab_visaInformation = new System.Windows.Forms.TabPage();
            this.pnl_visaInfoTwo = new System.Windows.Forms.Panel();
            this.txt_purposeOfVisit = new System.Windows.Forms.TextBox();
            this.dtp_timeOfArrival = new System.Windows.Forms.DateTimePicker();
            this.txt_departureTransportation = new System.Windows.Forms.TextBox();
            this.lbl_dateOfArrival = new System.Windows.Forms.Label();
            this.txt_goingTo = new System.Windows.Forms.TextBox();
            this.lbl_timeOfArrival = new System.Windows.Forms.Label();
            this.lbl_purposeOfVisit = new System.Windows.Forms.Label();
            this.dtp_dateOfArrival = new System.Windows.Forms.DateTimePicker();
            this.lbl_goingTo = new System.Windows.Forms.Label();
            this.lbl_departureTransportation = new System.Windows.Forms.Label();
            this.pnl_visaInfoOne = new System.Windows.Forms.Panel();
            this.txt_arrivedForm = new System.Windows.Forms.TextBox();
            this.lbl_serialNumber = new System.Windows.Forms.Label();
            this.txt_visaIssuePlace = new System.Windows.Forms.TextBox();
            this.lbl_visaNumber = new System.Windows.Forms.Label();
            this.txt_visaNumber = new System.Windows.Forms.TextBox();
            this.lbl_visaDate = new System.Windows.Forms.Label();
            this.dtp_visaDate = new System.Windows.Forms.DateTimePicker();
            this.lbl_vissaIssuePlace = new System.Windows.Forms.Label();
            this.txt_serialNumber = new System.Windows.Forms.TextBox();
            this.lbl_arrivedForm = new System.Windows.Forms.Label();
<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> 522a8075ec8125153832dce14daa61967b8a869e
            this.dgv_billingSummary = new System.Windows.Forms.DataGridView();
            this.col_Date = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_Particulars = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_Qty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_Unit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_Rate_Cost = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_TotalAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label1 = new System.Windows.Forms.Label();
<<<<<<< HEAD
=======
=======
            this.dgv_listOfRooms = new System.Windows.Forms.DataGridView();
            this.col_RoomType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_Available = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_Quantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_RoomRate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_Adult = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_Child = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_Amount = new System.Windows.Forms.DataGridViewTextBoxColumn();
>>>>>>> 3931348c34a4e1b1de18778096fd19dc0e91573a
>>>>>>> 522a8075ec8125153832dce14daa61967b8a869e
            this.guestFolio_panel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pcb_guestFolio)).BeginInit();
            this.pnl_guestFolio.SuspendLayout();
            this.tabControl2.SuspendLayout();
            this.tab_guestRemarks.SuspendLayout();
            this.pnl_guestRemarks.SuspendLayout();
            this.tab_billingSummary.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tab_guestInformation.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pcb_search)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcb_info)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcb_plusButton)).BeginInit();
            this.tab_settlementOptions.SuspendLayout();
            this.pnl_stayInformation.SuspendLayout();
            this.stayInformation_panel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pcb_reloadGuestFolio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcb_searchFolioNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcb_addBillTo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcb_searchBillTo)).BeginInit();
            this.tab_visaInformation.SuspendLayout();
            this.pnl_visaInfoTwo.SuspendLayout();
            this.pnl_visaInfoOne.SuspendLayout();
<<<<<<< HEAD
            ((System.ComponentModel.ISupportInitialize)(this.dgv_billingSummary)).BeginInit();
=======
<<<<<<< HEAD
            ((System.ComponentModel.ISupportInitialize)(this.dgv_billingSummary)).BeginInit();
=======
            ((System.ComponentModel.ISupportInitialize)(this.dgv_listOfRooms)).BeginInit();
>>>>>>> 3931348c34a4e1b1de18778096fd19dc0e91573a
>>>>>>> 522a8075ec8125153832dce14daa61967b8a869e
            this.SuspendLayout();
            // 
            // guestFolio_panel
            // 
            this.guestFolio_panel.Controls.Add(this.lbl_guestFolio);
            this.guestFolio_panel.Controls.Add(this.pcb_guestFolio);
            this.guestFolio_panel.Dock = System.Windows.Forms.DockStyle.Top;
            this.guestFolio_panel.Location = new System.Drawing.Point(0, 0);
            this.guestFolio_panel.Name = "guestFolio_panel";
            this.guestFolio_panel.Size = new System.Drawing.Size(1300, 35);
            this.guestFolio_panel.TabIndex = 1;
            this.guestFolio_panel.Paint += new System.Windows.Forms.PaintEventHandler(this.guestFolio_panel_Paint);
            // 
            // lbl_guestFolio
            // 
            this.lbl_guestFolio.AutoSize = true;
            this.lbl_guestFolio.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.lbl_guestFolio.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_guestFolio.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(126)))), ((int)(((byte)(57)))));
            this.lbl_guestFolio.Location = new System.Drawing.Point(37, 9);
            this.lbl_guestFolio.Name = "lbl_guestFolio";
            this.lbl_guestFolio.Size = new System.Drawing.Size(101, 17);
            this.lbl_guestFolio.TabIndex = 0;
            this.lbl_guestFolio.Text = "GUEST FOLIO";
            // 
            // pcb_guestFolio
            // 
            this.pcb_guestFolio.BackgroundImage = global::maxxis.Properties.Resources.Guest_Information;
            this.pcb_guestFolio.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pcb_guestFolio.Dock = System.Windows.Forms.DockStyle.Left;
            this.pcb_guestFolio.Location = new System.Drawing.Point(0, 0);
            this.pcb_guestFolio.Name = "pcb_guestFolio";
            this.pcb_guestFolio.Size = new System.Drawing.Size(41, 35);
            this.pcb_guestFolio.TabIndex = 1;
            this.pcb_guestFolio.TabStop = false;
            // 
            // pnl_guestFolio
            // 
            this.pnl_guestFolio.AutoScroll = true;
            this.pnl_guestFolio.BackColor = System.Drawing.Color.Silver;
<<<<<<< HEAD
            this.pnl_guestFolio.Controls.Add(this.label1);
            this.pnl_guestFolio.Controls.Add(this.btn_checkOut);
=======
<<<<<<< HEAD
            this.pnl_guestFolio.Controls.Add(this.label1);
            this.pnl_guestFolio.Controls.Add(this.btn_checkOut);
=======
            this.pnl_guestFolio.Controls.Add(this.btn_checkOut);
            this.pnl_guestFolio.Controls.Add(this.btn_GoBack);
>>>>>>> 3931348c34a4e1b1de18778096fd19dc0e91573a
>>>>>>> 522a8075ec8125153832dce14daa61967b8a869e
            this.pnl_guestFolio.Controls.Add(this.tabControl2);
            this.pnl_guestFolio.Controls.Add(this.btn_GoBack);
            this.pnl_guestFolio.Controls.Add(this.tabControl1);
            this.pnl_guestFolio.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnl_guestFolio.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pnl_guestFolio.Location = new System.Drawing.Point(0, 35);
            this.pnl_guestFolio.Name = "pnl_guestFolio";
<<<<<<< HEAD
            this.pnl_guestFolio.Size = new System.Drawing.Size(1300, 695);
=======
<<<<<<< HEAD
            this.pnl_guestFolio.Size = new System.Drawing.Size(1300, 695);
=======
            this.pnl_guestFolio.Size = new System.Drawing.Size(666, 695);
>>>>>>> 3931348c34a4e1b1de18778096fd19dc0e91573a
>>>>>>> 522a8075ec8125153832dce14daa61967b8a869e
            this.pnl_guestFolio.TabIndex = 2;
            // 
            // btn_checkOut
            // 
            this.btn_checkOut.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.btn_checkOut.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btn_checkOut.FlatAppearance.BorderSize = 0;
            this.btn_checkOut.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_checkOut.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_checkOut.ForeColor = System.Drawing.Color.White;
            this.btn_checkOut.Image = global::maxxis.Properties.Resources.guestFolio_Info;
            this.btn_checkOut.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
<<<<<<< HEAD
            this.btn_checkOut.Location = new System.Drawing.Point(638, 776);
=======
<<<<<<< HEAD
            this.btn_checkOut.Location = new System.Drawing.Point(638, 776);
=======
            this.btn_checkOut.Location = new System.Drawing.Point(265, 657);
>>>>>>> 3931348c34a4e1b1de18778096fd19dc0e91573a
>>>>>>> 522a8075ec8125153832dce14daa61967b8a869e
            this.btn_checkOut.Name = "btn_checkOut";
            this.btn_checkOut.Size = new System.Drawing.Size(128, 35);
            this.btn_checkOut.TabIndex = 3;
            this.btn_checkOut.Text = "CHECK OUT";
            this.btn_checkOut.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_checkOut.UseVisualStyleBackColor = false;
            // 
            // btn_GoBack
            // 
            this.btn_GoBack.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.btn_GoBack.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btn_GoBack.FlatAppearance.BorderSize = 0;
            this.btn_GoBack.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_GoBack.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_GoBack.ForeColor = System.Drawing.Color.White;
            this.btn_GoBack.Image = global::maxxis.Properties.Resources.Go_Back;
            this.btn_GoBack.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
<<<<<<< HEAD
            this.btn_GoBack.Location = new System.Drawing.Point(490, 776);
=======
<<<<<<< HEAD
            this.btn_GoBack.Location = new System.Drawing.Point(490, 776);
=======
            this.btn_GoBack.Location = new System.Drawing.Point(117, 657);
>>>>>>> 3931348c34a4e1b1de18778096fd19dc0e91573a
>>>>>>> 522a8075ec8125153832dce14daa61967b8a869e
            this.btn_GoBack.Name = "btn_GoBack";
            this.btn_GoBack.Size = new System.Drawing.Size(128, 35);
            this.btn_GoBack.TabIndex = 2;
            this.btn_GoBack.Text = "GO BACK";
            this.btn_GoBack.UseVisualStyleBackColor = false;
            // 
            // tabControl2
            // 
            this.tabControl2.Appearance = System.Windows.Forms.TabAppearance.FlatButtons;
            this.tabControl2.Controls.Add(this.tab_guestRemarks);
            this.tabControl2.Controls.Add(this.tab_billingSummary);
            this.tabControl2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabControl2.Location = new System.Drawing.Point(318, 427);
            this.tabControl2.Name = "tabControl2";
            this.tabControl2.Padding = new System.Drawing.Point(20, 10);
            this.tabControl2.SelectedIndex = 0;
<<<<<<< HEAD
            this.tabControl2.Size = new System.Drawing.Size(641, 329);
=======
<<<<<<< HEAD
            this.tabControl2.Size = new System.Drawing.Size(641, 329);
=======
            this.tabControl2.Size = new System.Drawing.Size(641, 218);
>>>>>>> 3931348c34a4e1b1de18778096fd19dc0e91573a
>>>>>>> 522a8075ec8125153832dce14daa61967b8a869e
            this.tabControl2.SizeMode = System.Windows.Forms.TabSizeMode.FillToRight;
            this.tabControl2.TabIndex = 1;
            // 
            // tab_guestRemarks
            // 
            this.tab_guestRemarks.Controls.Add(this.pnl_guestRemarks);
            this.tab_guestRemarks.Location = new System.Drawing.Point(4, 42);
            this.tab_guestRemarks.Name = "tab_guestRemarks";
            this.tab_guestRemarks.Padding = new System.Windows.Forms.Padding(3);
<<<<<<< HEAD
            this.tab_guestRemarks.Size = new System.Drawing.Size(633, 283);
=======
<<<<<<< HEAD
            this.tab_guestRemarks.Size = new System.Drawing.Size(633, 283);
=======
            this.tab_guestRemarks.Size = new System.Drawing.Size(633, 172);
>>>>>>> 3931348c34a4e1b1de18778096fd19dc0e91573a
>>>>>>> 522a8075ec8125153832dce14daa61967b8a869e
            this.tab_guestRemarks.TabIndex = 0;
            this.tab_guestRemarks.Text = "Guest Remark";
            this.tab_guestRemarks.UseVisualStyleBackColor = true;
            this.tab_guestRemarks.Click += new System.EventHandler(this.tab_guestRemarks_Click);
            // 
            // pnl_guestRemarks
            // 
            this.pnl_guestRemarks.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnl_guestRemarks.Controls.Add(this.txt_guestRemarks);
            this.pnl_guestRemarks.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnl_guestRemarks.Location = new System.Drawing.Point(3, 3);
            this.pnl_guestRemarks.Name = "pnl_guestRemarks";
<<<<<<< HEAD
            this.pnl_guestRemarks.Size = new System.Drawing.Size(627, 277);
=======
<<<<<<< HEAD
            this.pnl_guestRemarks.Size = new System.Drawing.Size(627, 277);
=======
            this.pnl_guestRemarks.Size = new System.Drawing.Size(627, 166);
>>>>>>> 3931348c34a4e1b1de18778096fd19dc0e91573a
>>>>>>> 522a8075ec8125153832dce14daa61967b8a869e
            this.pnl_guestRemarks.TabIndex = 0;
            // 
            // txt_guestRemarks
            // 
            this.txt_guestRemarks.BackColor = System.Drawing.Color.Silver;
            this.txt_guestRemarks.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_guestRemarks.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txt_guestRemarks.Location = new System.Drawing.Point(0, 0);
            this.txt_guestRemarks.Multiline = true;
            this.txt_guestRemarks.Name = "txt_guestRemarks";
            this.txt_guestRemarks.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
<<<<<<< HEAD
            this.txt_guestRemarks.Size = new System.Drawing.Size(625, 275);
=======
<<<<<<< HEAD
            this.txt_guestRemarks.Size = new System.Drawing.Size(625, 275);
=======
            this.txt_guestRemarks.Size = new System.Drawing.Size(625, 164);
>>>>>>> 3931348c34a4e1b1de18778096fd19dc0e91573a
>>>>>>> 522a8075ec8125153832dce14daa61967b8a869e
            this.txt_guestRemarks.TabIndex = 0;
            // 
            // tab_billingSummary
            // 
            this.tab_billingSummary.BackColor = System.Drawing.Color.Silver;
<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> 522a8075ec8125153832dce14daa61967b8a869e
            this.tab_billingSummary.Controls.Add(this.dgv_billingSummary);
            this.tab_billingSummary.Location = new System.Drawing.Point(4, 42);
            this.tab_billingSummary.Name = "tab_billingSummary";
            this.tab_billingSummary.Padding = new System.Windows.Forms.Padding(3);
            this.tab_billingSummary.Size = new System.Drawing.Size(633, 283);
<<<<<<< HEAD
=======
=======
            this.tab_billingSummary.Controls.Add(this.dgv_listOfRooms);
            this.tab_billingSummary.Location = new System.Drawing.Point(4, 42);
            this.tab_billingSummary.Name = "tab_billingSummary";
            this.tab_billingSummary.Padding = new System.Windows.Forms.Padding(3);
            this.tab_billingSummary.Size = new System.Drawing.Size(633, 172);
>>>>>>> 3931348c34a4e1b1de18778096fd19dc0e91573a
>>>>>>> 522a8075ec8125153832dce14daa61967b8a869e
            this.tab_billingSummary.TabIndex = 1;
            this.tab_billingSummary.Text = "Billing Summary";
            // 
            // tabControl1
            // 
            this.tabControl1.Appearance = System.Windows.Forms.TabAppearance.FlatButtons;
            this.tabControl1.Controls.Add(this.tab_guestInformation);
            this.tabControl1.Controls.Add(this.tab_settlementOptions);
            this.tabControl1.Controls.Add(this.tab_visaInformation);
            this.tabControl1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabControl1.Location = new System.Drawing.Point(318, 6);
            this.tabControl1.Multiline = true;
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.Padding = new System.Drawing.Point(20, 8);
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(641, 415);
            this.tabControl1.SizeMode = System.Windows.Forms.TabSizeMode.FillToRight;
            this.tabControl1.TabIndex = 0;
            // 
            // tab_guestInformation
            // 
            this.tab_guestInformation.BackColor = System.Drawing.Color.Silver;
            this.tab_guestInformation.Controls.Add(this.cmb_homeCountry);
            this.tab_guestInformation.Controls.Add(this.lbl_homeCountry);
            this.tab_guestInformation.Controls.Add(this.lbl_companyAgency);
            this.tab_guestInformation.Controls.Add(this.txt_companyAgency);
            this.tab_guestInformation.Controls.Add(this.lbl_dateOfBirth);
            this.tab_guestInformation.Controls.Add(this.dtb_dateOfBirth);
            this.tab_guestInformation.Controls.Add(this.lbl_province);
            this.tab_guestInformation.Controls.Add(this.txt_province);
            this.tab_guestInformation.Controls.Add(this.lbl_city);
            this.tab_guestInformation.Controls.Add(this.txt_city);
            this.tab_guestInformation.Controls.Add(this.lbl_address);
            this.tab_guestInformation.Controls.Add(this.txt_address);
            this.tab_guestInformation.Controls.Add(this.txt_middleName);
            this.tab_guestInformation.Controls.Add(this.lbl_middleName);
            this.tab_guestInformation.Controls.Add(this.lbl_title);
            this.tab_guestInformation.Controls.Add(this.lbl_email);
            this.tab_guestInformation.Controls.Add(this.txt_email);
            this.tab_guestInformation.Controls.Add(this.lbl_contactDetails);
            this.tab_guestInformation.Controls.Add(this.txt_contactDetails);
            this.tab_guestInformation.Controls.Add(this.lbl_last_name);
            this.tab_guestInformation.Controls.Add(this.lbl_showRates);
            this.tab_guestInformation.Controls.Add(this.pcb_search);
            this.tab_guestInformation.Controls.Add(this.pcb_info);
            this.tab_guestInformation.Controls.Add(this.pcb_plusButton);
            this.tab_guestInformation.Controls.Add(this.txt_firstname);
            this.tab_guestInformation.Controls.Add(this.comboBox1);
            this.tab_guestInformation.Controls.Add(this.lbl_firstname);
            this.tab_guestInformation.Controls.Add(this.txt_lastname);
            this.tab_guestInformation.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tab_guestInformation.Location = new System.Drawing.Point(4, 38);
            this.tab_guestInformation.Name = "tab_guestInformation";
            this.tab_guestInformation.Padding = new System.Windows.Forms.Padding(3);
            this.tab_guestInformation.Size = new System.Drawing.Size(633, 373);
            this.tab_guestInformation.TabIndex = 0;
            this.tab_guestInformation.Text = "Guest Information";
            this.tab_guestInformation.Click += new System.EventHandler(this.tab_guestInformation_Click);
            // 
            // cmb_homeCountry
            // 
            this.cmb_homeCountry.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmb_homeCountry.FormattingEnabled = true;
            this.cmb_homeCountry.Location = new System.Drawing.Point(341, 239);
            this.cmb_homeCountry.Name = "cmb_homeCountry";
            this.cmb_homeCountry.Size = new System.Drawing.Size(218, 24);
            this.cmb_homeCountry.TabIndex = 76;
            // 
            // lbl_homeCountry
            // 
            this.lbl_homeCountry.AutoSize = true;
            this.lbl_homeCountry.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_homeCountry.Location = new System.Drawing.Point(338, 221);
            this.lbl_homeCountry.Name = "lbl_homeCountry";
            this.lbl_homeCountry.Size = new System.Drawing.Size(95, 16);
            this.lbl_homeCountry.TabIndex = 75;
            this.lbl_homeCountry.Text = "Home Country";
            // 
            // lbl_companyAgency
            // 
            this.lbl_companyAgency.AutoSize = true;
            this.lbl_companyAgency.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_companyAgency.Location = new System.Drawing.Point(21, 275);
            this.lbl_companyAgency.Name = "lbl_companyAgency";
            this.lbl_companyAgency.Size = new System.Drawing.Size(115, 16);
            this.lbl_companyAgency.TabIndex = 70;
            this.lbl_companyAgency.Text = "Company/Agency";
            // 
            // txt_companyAgency
            // 
            this.txt_companyAgency.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_companyAgency.Location = new System.Drawing.Point(24, 294);
            this.txt_companyAgency.Name = "txt_companyAgency";
            this.txt_companyAgency.Size = new System.Drawing.Size(300, 23);
            this.txt_companyAgency.TabIndex = 69;
            // 
            // lbl_dateOfBirth
            // 
            this.lbl_dateOfBirth.AutoSize = true;
            this.lbl_dateOfBirth.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_dateOfBirth.Location = new System.Drawing.Point(385, 57);
            this.lbl_dateOfBirth.Name = "lbl_dateOfBirth";
            this.lbl_dateOfBirth.Size = new System.Drawing.Size(84, 16);
            this.lbl_dateOfBirth.TabIndex = 68;
            this.lbl_dateOfBirth.Text = "Date of Birth";
            // 
            // dtb_dateOfBirth
            // 
            this.dtb_dateOfBirth.CalendarTitleBackColor = System.Drawing.SystemColors.ControlText;
            this.dtb_dateOfBirth.CalendarTitleForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.dtb_dateOfBirth.CustomFormat = "MMMM dd, yyyy    -dddd";
            this.dtb_dateOfBirth.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtb_dateOfBirth.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtb_dateOfBirth.Location = new System.Drawing.Point(388, 76);
            this.dtb_dateOfBirth.Name = "dtb_dateOfBirth";
            this.dtb_dateOfBirth.Size = new System.Drawing.Size(220, 23);
            this.dtb_dateOfBirth.TabIndex = 67;
            // 
            // lbl_province
            // 
            this.lbl_province.AutoSize = true;
            this.lbl_province.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_province.Location = new System.Drawing.Point(21, 220);
            this.lbl_province.Name = "lbl_province";
            this.lbl_province.Size = new System.Drawing.Size(59, 16);
            this.lbl_province.TabIndex = 66;
            this.lbl_province.Text = "Province";
            // 
            // txt_province
            // 
            this.txt_province.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_province.Location = new System.Drawing.Point(24, 239);
            this.txt_province.Name = "txt_province";
            this.txt_province.Size = new System.Drawing.Size(300, 23);
            this.txt_province.TabIndex = 65;
            // 
            // lbl_city
            // 
            this.lbl_city.AutoSize = true;
            this.lbl_city.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_city.Location = new System.Drawing.Point(338, 166);
            this.lbl_city.Name = "lbl_city";
            this.lbl_city.Size = new System.Drawing.Size(32, 16);
            this.lbl_city.TabIndex = 64;
            this.lbl_city.Text = "City";
            // 
            // txt_city
            // 
            this.txt_city.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_city.Location = new System.Drawing.Point(341, 185);
            this.txt_city.Name = "txt_city";
            this.txt_city.Size = new System.Drawing.Size(218, 23);
            this.txt_city.TabIndex = 63;
            // 
            // lbl_address
            // 
            this.lbl_address.AutoSize = true;
            this.lbl_address.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_address.Location = new System.Drawing.Point(21, 166);
            this.lbl_address.Name = "lbl_address";
            this.lbl_address.Size = new System.Drawing.Size(58, 16);
            this.lbl_address.TabIndex = 62;
            this.lbl_address.Text = "Address";
            // 
            // txt_address
            // 
            this.txt_address.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_address.Location = new System.Drawing.Point(24, 185);
            this.txt_address.Name = "txt_address";
            this.txt_address.Size = new System.Drawing.Size(300, 23);
            this.txt_address.TabIndex = 61;
            // 
            // txt_middleName
            // 
            this.txt_middleName.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_middleName.Location = new System.Drawing.Point(24, 76);
            this.txt_middleName.Name = "txt_middleName";
            this.txt_middleName.Size = new System.Drawing.Size(220, 23);
            this.txt_middleName.TabIndex = 60;
            // 
            // lbl_middleName
            // 
            this.lbl_middleName.AutoSize = true;
            this.lbl_middleName.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_middleName.Location = new System.Drawing.Point(21, 59);
            this.lbl_middleName.Name = "lbl_middleName";
            this.lbl_middleName.Size = new System.Drawing.Size(87, 16);
            this.lbl_middleName.TabIndex = 59;
            this.lbl_middleName.Text = "Middle Name";
            // 
            // lbl_title
            // 
            this.lbl_title.AutoSize = true;
            this.lbl_title.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_title.Location = new System.Drawing.Point(21, 6);
            this.lbl_title.Name = "lbl_title";
            this.lbl_title.Size = new System.Drawing.Size(33, 16);
            this.lbl_title.TabIndex = 58;
            this.lbl_title.Text = "Title";
            // 
            // lbl_email
            // 
            this.lbl_email.AutoSize = true;
            this.lbl_email.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_email.Location = new System.Drawing.Point(239, 115);
            this.lbl_email.Name = "lbl_email";
            this.lbl_email.Size = new System.Drawing.Size(40, 16);
            this.lbl_email.TabIndex = 57;
            this.lbl_email.Text = "Email";
            // 
            // txt_email
            // 
            this.txt_email.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_email.Location = new System.Drawing.Point(242, 134);
            this.txt_email.Name = "txt_email";
            this.txt_email.Size = new System.Drawing.Size(291, 23);
            this.txt_email.TabIndex = 56;
            // 
            // lbl_contactDetails
            // 
            this.lbl_contactDetails.AutoSize = true;
            this.lbl_contactDetails.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_contactDetails.Location = new System.Drawing.Point(21, 115);
            this.lbl_contactDetails.Name = "lbl_contactDetails";
            this.lbl_contactDetails.Size = new System.Drawing.Size(101, 16);
            this.lbl_contactDetails.TabIndex = 55;
            this.lbl_contactDetails.Text = "Contact Details";
            // 
            // txt_contactDetails
            // 
            this.txt_contactDetails.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_contactDetails.Location = new System.Drawing.Point(24, 134);
            this.txt_contactDetails.Name = "txt_contactDetails";
            this.txt_contactDetails.Size = new System.Drawing.Size(201, 23);
            this.txt_contactDetails.TabIndex = 54;
            // 
            // lbl_last_name
            // 
            this.lbl_last_name.AutoSize = true;
            this.lbl_last_name.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_last_name.Location = new System.Drawing.Point(115, 6);
            this.lbl_last_name.Name = "lbl_last_name";
            this.lbl_last_name.Size = new System.Drawing.Size(72, 16);
            this.lbl_last_name.TabIndex = 53;
            this.lbl_last_name.Text = "Last Name";
            // 
            // lbl_showRates
            // 
            this.lbl_showRates.AutoSize = true;
            this.lbl_showRates.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_showRates.Location = new System.Drawing.Point(518, 355);
            this.lbl_showRates.Name = "lbl_showRates";
            this.lbl_showRates.Size = new System.Drawing.Size(90, 16);
            this.lbl_showRates.TabIndex = 16;
            this.lbl_showRates.Text = "Show More +";
            this.lbl_showRates.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl_showRates.Click += new System.EventHandler(this.lbl_showRates_Click);
            // 
            // pcb_search
            // 
            this.pcb_search.BackColor = System.Drawing.Color.Silver;
            this.pcb_search.BackgroundImage = global::maxxis.Properties.Resources.search;
            this.pcb_search.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pcb_search.Location = new System.Drawing.Point(249, 72);
            this.pcb_search.Name = "pcb_search";
            this.pcb_search.Size = new System.Drawing.Size(34, 31);
            this.pcb_search.TabIndex = 15;
            this.pcb_search.TabStop = false;
            // 
            // pcb_info
            // 
            this.pcb_info.BackColor = System.Drawing.Color.Silver;
            this.pcb_info.BackgroundImage = global::maxxis.Properties.Resources.guestFolio_Info;
            this.pcb_info.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pcb_info.Location = new System.Drawing.Point(337, 72);
            this.pcb_info.Name = "pcb_info";
            this.pcb_info.Size = new System.Drawing.Size(34, 31);
            this.pcb_info.TabIndex = 14;
            this.pcb_info.TabStop = false;
            // 
            // pcb_plusButton
            // 
            this.pcb_plusButton.BackColor = System.Drawing.Color.Silver;
            this.pcb_plusButton.BackgroundImage = global::maxxis.Properties.Resources.guestFolio_plus;
            this.pcb_plusButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pcb_plusButton.Location = new System.Drawing.Point(293, 72);
            this.pcb_plusButton.Name = "pcb_plusButton";
            this.pcb_plusButton.Size = new System.Drawing.Size(33, 31);
            this.pcb_plusButton.TabIndex = 13;
            this.pcb_plusButton.TabStop = false;
            // 
            // txt_firstname
            // 
            this.txt_firstname.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_firstname.Location = new System.Drawing.Point(313, 23);
            this.txt_firstname.Name = "txt_firstname";
            this.txt_firstname.Size = new System.Drawing.Size(220, 23);
            this.txt_firstname.TabIndex = 12;
            this.txt_firstname.TextChanged += new System.EventHandler(this.txt_firstname_TextChanged);
            // 
            // comboBox1
            // 
            this.comboBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(24, 23);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(78, 24);
            this.comboBox1.TabIndex = 11;
            // 
            // lbl_firstname
            // 
            this.lbl_firstname.AutoSize = true;
            this.lbl_firstname.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_firstname.Location = new System.Drawing.Point(310, 6);
            this.lbl_firstname.Name = "lbl_firstname";
            this.lbl_firstname.Size = new System.Drawing.Size(73, 16);
            this.lbl_firstname.TabIndex = 10;
            this.lbl_firstname.Text = "First Name";
            // 
            // txt_lastname
            // 
            this.txt_lastname.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_lastname.Location = new System.Drawing.Point(118, 23);
            this.txt_lastname.Name = "txt_lastname";
            this.txt_lastname.Size = new System.Drawing.Size(178, 23);
            this.txt_lastname.TabIndex = 9;
            // 
            // tab_settlementOptions
            // 
            this.tab_settlementOptions.BackColor = System.Drawing.Color.Silver;
            this.tab_settlementOptions.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.tab_settlementOptions.Controls.Add(this.lbl_modeOfPayment);
            this.tab_settlementOptions.Controls.Add(this.rbtn_partialCredit);
            this.tab_settlementOptions.Controls.Add(this.rbtn_credit);
            this.tab_settlementOptions.Controls.Add(this.rbtn_cash);
            this.tab_settlementOptions.Controls.Add(this.pnl_stayInformation);
            this.tab_settlementOptions.Controls.Add(this.txt_folioNumber);
            this.tab_settlementOptions.Controls.Add(this.lbl_folioNumber);
            this.tab_settlementOptions.Controls.Add(this.txt_billTo);
            this.tab_settlementOptions.Controls.Add(this.lbl_billTo);
            this.tab_settlementOptions.Controls.Add(this.txt_expire);
            this.tab_settlementOptions.Controls.Add(this.lbl_expire);
            this.tab_settlementOptions.Controls.Add(this.txt_cardNumber);
            this.tab_settlementOptions.Controls.Add(this.lbl_cardNumber);
            this.tab_settlementOptions.Controls.Add(this.cmb_type);
            this.tab_settlementOptions.Controls.Add(this.lbl_type);
            this.tab_settlementOptions.Controls.Add(this.pcb_reloadGuestFolio);
            this.tab_settlementOptions.Controls.Add(this.pcb_searchFolioNumber);
            this.tab_settlementOptions.Controls.Add(this.pcb_addBillTo);
            this.tab_settlementOptions.Controls.Add(this.pcb_searchBillTo);
            this.tab_settlementOptions.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.tab_settlementOptions.Location = new System.Drawing.Point(4, 38);
            this.tab_settlementOptions.Name = "tab_settlementOptions";
            this.tab_settlementOptions.Padding = new System.Windows.Forms.Padding(3);
            this.tab_settlementOptions.Size = new System.Drawing.Size(633, 373);
            this.tab_settlementOptions.TabIndex = 1;
            this.tab_settlementOptions.Text = "Settlement Options";
            // 
            // lbl_modeOfPayment
            // 
            this.lbl_modeOfPayment.AutoSize = true;
            this.lbl_modeOfPayment.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_modeOfPayment.Location = new System.Drawing.Point(7, 11);
            this.lbl_modeOfPayment.Name = "lbl_modeOfPayment";
            this.lbl_modeOfPayment.Size = new System.Drawing.Size(135, 19);
            this.lbl_modeOfPayment.TabIndex = 48;
            this.lbl_modeOfPayment.Text = "Mode Of Payment:";
            // 
            // rbtn_partialCredit
            // 
            this.rbtn_partialCredit.AutoSize = true;
<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> 522a8075ec8125153832dce14daa61967b8a869e
            this.rbtn_partialCredit.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtn_partialCredit.Location = new System.Drawing.Point(358, 11);
            this.rbtn_partialCredit.Name = "rbtn_partialCredit";
            this.rbtn_partialCredit.Size = new System.Drawing.Size(104, 20);
<<<<<<< HEAD
=======
=======
            this.rbtn_partialCredit.Location = new System.Drawing.Point(358, 11);
            this.rbtn_partialCredit.Name = "rbtn_partialCredit";
            this.rbtn_partialCredit.Size = new System.Drawing.Size(107, 21);
>>>>>>> 3931348c34a4e1b1de18778096fd19dc0e91573a
>>>>>>> 522a8075ec8125153832dce14daa61967b8a869e
            this.rbtn_partialCredit.TabIndex = 47;
            this.rbtn_partialCredit.Text = "Partial Credit";
            this.rbtn_partialCredit.UseVisualStyleBackColor = true;
            // 
            // rbtn_credit
            // 
            this.rbtn_credit.AutoSize = true;
<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> 522a8075ec8125153832dce14daa61967b8a869e
            this.rbtn_credit.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtn_credit.Location = new System.Drawing.Point(247, 11);
            this.rbtn_credit.Name = "rbtn_credit";
            this.rbtn_credit.Size = new System.Drawing.Size(63, 20);
<<<<<<< HEAD
=======
=======
            this.rbtn_credit.Location = new System.Drawing.Point(247, 11);
            this.rbtn_credit.Name = "rbtn_credit";
            this.rbtn_credit.Size = new System.Drawing.Size(63, 21);
>>>>>>> 3931348c34a4e1b1de18778096fd19dc0e91573a
>>>>>>> 522a8075ec8125153832dce14daa61967b8a869e
            this.rbtn_credit.TabIndex = 46;
            this.rbtn_credit.Text = "Credit";
            this.rbtn_credit.UseVisualStyleBackColor = true;
            // 
            // rbtn_cash
            // 
            this.rbtn_cash.AutoSize = true;
            this.rbtn_cash.Checked = true;
<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> 522a8075ec8125153832dce14daa61967b8a869e
            this.rbtn_cash.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtn_cash.Location = new System.Drawing.Point(145, 11);
            this.rbtn_cash.Name = "rbtn_cash";
            this.rbtn_cash.Size = new System.Drawing.Size(58, 20);
<<<<<<< HEAD
=======
=======
            this.rbtn_cash.Location = new System.Drawing.Point(145, 11);
            this.rbtn_cash.Name = "rbtn_cash";
            this.rbtn_cash.Size = new System.Drawing.Size(58, 21);
>>>>>>> 3931348c34a4e1b1de18778096fd19dc0e91573a
>>>>>>> 522a8075ec8125153832dce14daa61967b8a869e
            this.rbtn_cash.TabIndex = 45;
            this.rbtn_cash.TabStop = true;
            this.rbtn_cash.Text = "Cash";
            this.rbtn_cash.UseVisualStyleBackColor = true;
            // 
            // pnl_stayInformation
            // 
            this.pnl_stayInformation.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnl_stayInformation.Controls.Add(this.dtp_departureTime);
            this.pnl_stayInformation.Controls.Add(this.dtp_arrivalTime);
            this.pnl_stayInformation.Controls.Add(this.dtp_departureDate);
            this.pnl_stayInformation.Controls.Add(this.dtp_arrivalDate);
            this.pnl_stayInformation.Controls.Add(this.dom_child);
            this.pnl_stayInformation.Controls.Add(this.dom_adults);
            this.pnl_stayInformation.Controls.Add(this.dom_numberOfNights);
            this.pnl_stayInformation.Controls.Add(this.lbl_child);
            this.pnl_stayInformation.Controls.Add(this.lbl_adults);
            this.pnl_stayInformation.Controls.Add(this.lbl_numberOfNights);
            this.pnl_stayInformation.Controls.Add(this.lbl_departure);
            this.pnl_stayInformation.Controls.Add(this.lbl_arrival);
            this.pnl_stayInformation.Controls.Add(this.stayInformation_panel);
            this.pnl_stayInformation.Location = new System.Drawing.Point(6, 204);
            this.pnl_stayInformation.Name = "pnl_stayInformation";
            this.pnl_stayInformation.Size = new System.Drawing.Size(617, 164);
            this.pnl_stayInformation.TabIndex = 24;
            this.pnl_stayInformation.Paint += new System.Windows.Forms.PaintEventHandler(this.panel4_Paint);
            // 
            // dtp_departureTime
            // 
            this.dtp_departureTime.CustomFormat = "  hh:mm:ss tt";
            this.dtp_departureTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtp_departureTime.Location = new System.Drawing.Point(397, 82);
            this.dtp_departureTime.Name = "dtp_departureTime";
            this.dtp_departureTime.ShowCheckBox = true;
            this.dtp_departureTime.ShowUpDown = true;
            this.dtp_departureTime.Size = new System.Drawing.Size(138, 23);
            this.dtp_departureTime.TabIndex = 41;
            // 
            // dtp_arrivalTime
            // 
            this.dtp_arrivalTime.CustomFormat = "  hh:mm:ss tt";
            this.dtp_arrivalTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtp_arrivalTime.Location = new System.Drawing.Point(397, 41);
            this.dtp_arrivalTime.Name = "dtp_arrivalTime";
            this.dtp_arrivalTime.ShowCheckBox = true;
            this.dtp_arrivalTime.ShowUpDown = true;
            this.dtp_arrivalTime.Size = new System.Drawing.Size(138, 23);
            this.dtp_arrivalTime.TabIndex = 40;
            // 
            // dtp_departureDate
            // 
            this.dtp_departureDate.CustomFormat = "MMMM dd, yyyy    -dddd";
            this.dtp_departureDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtp_departureDate.Location = new System.Drawing.Point(123, 83);
            this.dtp_departureDate.Name = "dtp_departureDate";
            this.dtp_departureDate.ShowCheckBox = true;
            this.dtp_departureDate.Size = new System.Drawing.Size(268, 23);
            this.dtp_departureDate.TabIndex = 39;
            // 
            // dtp_arrivalDate
            // 
            this.dtp_arrivalDate.CustomFormat = "MMMM dd, yyyy    -dddd";
            this.dtp_arrivalDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtp_arrivalDate.Location = new System.Drawing.Point(123, 41);
            this.dtp_arrivalDate.Name = "dtp_arrivalDate";
            this.dtp_arrivalDate.ShowCheckBox = true;
            this.dtp_arrivalDate.Size = new System.Drawing.Size(268, 23);
            this.dtp_arrivalDate.TabIndex = 38;
            // 
            // dom_child
            // 
            this.dom_child.Location = new System.Drawing.Point(361, 125);
            this.dom_child.Name = "dom_child";
            this.dom_child.Size = new System.Drawing.Size(47, 23);
            this.dom_child.TabIndex = 37;
            this.dom_child.Text = "0";
            // 
            // dom_adults
            // 
            this.dom_adults.Location = new System.Drawing.Point(246, 125);
            this.dom_adults.Name = "dom_adults";
            this.dom_adults.Size = new System.Drawing.Size(47, 23);
            this.dom_adults.TabIndex = 36;
            this.dom_adults.Text = "1";
            // 
            // dom_numberOfNights
            // 
            this.dom_numberOfNights.Location = new System.Drawing.Point(123, 125);
            this.dom_numberOfNights.Name = "dom_numberOfNights";
            this.dom_numberOfNights.Size = new System.Drawing.Size(47, 23);
            this.dom_numberOfNights.TabIndex = 35;
            this.dom_numberOfNights.Text = "1";
            // 
            // lbl_child
            // 
            this.lbl_child.AutoSize = true;
            this.lbl_child.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_child.Location = new System.Drawing.Point(311, 125);
            this.lbl_child.Name = "lbl_child";
            this.lbl_child.Size = new System.Drawing.Size(44, 19);
            this.lbl_child.TabIndex = 34;
            this.lbl_child.Text = "Child";
            // 
            // lbl_adults
            // 
            this.lbl_adults.AutoSize = true;
            this.lbl_adults.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_adults.Location = new System.Drawing.Point(188, 125);
            this.lbl_adults.Name = "lbl_adults";
            this.lbl_adults.Size = new System.Drawing.Size(52, 19);
            this.lbl_adults.TabIndex = 31;
            this.lbl_adults.Text = "Adults";
            // 
            // lbl_numberOfNights
            // 
            this.lbl_numberOfNights.AutoSize = true;
            this.lbl_numberOfNights.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_numberOfNights.Location = new System.Drawing.Point(9, 125);
            this.lbl_numberOfNights.Name = "lbl_numberOfNights";
            this.lbl_numberOfNights.Size = new System.Drawing.Size(108, 19);
            this.lbl_numberOfNights.TabIndex = 27;
            this.lbl_numberOfNights.Text = "No. of Night(s)";
            // 
            // lbl_departure
            // 
            this.lbl_departure.AutoSize = true;
            this.lbl_departure.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_departure.Location = new System.Drawing.Point(9, 86);
            this.lbl_departure.Name = "lbl_departure";
            this.lbl_departure.Size = new System.Drawing.Size(76, 19);
            this.lbl_departure.TabIndex = 26;
            this.lbl_departure.Text = "Departure";
            // 
            // lbl_arrival
            // 
            this.lbl_arrival.AutoSize = true;
            this.lbl_arrival.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_arrival.Location = new System.Drawing.Point(9, 44);
            this.lbl_arrival.Name = "lbl_arrival";
            this.lbl_arrival.Size = new System.Drawing.Size(50, 19);
            this.lbl_arrival.TabIndex = 25;
            this.lbl_arrival.Text = "Arrival";
            // 
            // stayInformation_panel
            // 
            this.stayInformation_panel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.stayInformation_panel.Controls.Add(this.lbl_stayInformation);
            this.stayInformation_panel.Dock = System.Windows.Forms.DockStyle.Top;
            this.stayInformation_panel.Location = new System.Drawing.Point(0, 0);
            this.stayInformation_panel.Name = "stayInformation_panel";
            this.stayInformation_panel.Size = new System.Drawing.Size(615, 29);
            this.stayInformation_panel.TabIndex = 2;
            // 
            // lbl_stayInformation
            // 
            this.lbl_stayInformation.AutoSize = true;
            this.lbl_stayInformation.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.lbl_stayInformation.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_stayInformation.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(126)))), ((int)(((byte)(57)))));
            this.lbl_stayInformation.Location = new System.Drawing.Point(4, 7);
            this.lbl_stayInformation.Name = "lbl_stayInformation";
            this.lbl_stayInformation.Size = new System.Drawing.Size(110, 17);
            this.lbl_stayInformation.TabIndex = 0;
            this.lbl_stayInformation.Text = "Stay Information";
            // 
            // txt_folioNumber
            // 
            this.txt_folioNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_folioNumber.Location = new System.Drawing.Point(102, 161);
            this.txt_folioNumber.Name = "txt_folioNumber";
            this.txt_folioNumber.Size = new System.Drawing.Size(343, 23);
            this.txt_folioNumber.TabIndex = 21;
            // 
            // lbl_folioNumber
            // 
            this.lbl_folioNumber.AutoSize = true;
            this.lbl_folioNumber.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_folioNumber.Location = new System.Drawing.Point(7, 165);
            this.lbl_folioNumber.Name = "lbl_folioNumber";
            this.lbl_folioNumber.Size = new System.Drawing.Size(70, 19);
            this.lbl_folioNumber.TabIndex = 20;
            this.lbl_folioNumber.Text = "Folio No.";
            // 
            // txt_billTo
            // 
            this.txt_billTo.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_billTo.Location = new System.Drawing.Point(102, 123);
            this.txt_billTo.Name = "txt_billTo";
            this.txt_billTo.Size = new System.Drawing.Size(343, 23);
            this.txt_billTo.TabIndex = 17;
            // 
            // lbl_billTo
            // 
            this.lbl_billTo.AutoSize = true;
            this.lbl_billTo.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_billTo.Location = new System.Drawing.Point(7, 128);
            this.lbl_billTo.Name = "lbl_billTo";
            this.lbl_billTo.Size = new System.Drawing.Size(51, 19);
            this.lbl_billTo.TabIndex = 16;
            this.lbl_billTo.Text = "Bill To";
            // 
            // txt_expire
            // 
            this.txt_expire.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_expire.Location = new System.Drawing.Point(451, 83);
            this.txt_expire.Name = "txt_expire";
            this.txt_expire.Size = new System.Drawing.Size(78, 23);
            this.txt_expire.TabIndex = 15;
            // 
            // lbl_expire
            // 
            this.lbl_expire.AutoSize = true;
            this.lbl_expire.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_expire.Location = new System.Drawing.Point(395, 87);
            this.lbl_expire.Name = "lbl_expire";
            this.lbl_expire.Size = new System.Drawing.Size(50, 19);
            this.lbl_expire.TabIndex = 14;
            this.lbl_expire.Text = "Expire";
            // 
            // txt_cardNumber
            // 
            this.txt_cardNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_cardNumber.Location = new System.Drawing.Point(102, 83);
            this.txt_cardNumber.Name = "txt_cardNumber";
            this.txt_cardNumber.Size = new System.Drawing.Size(287, 23);
            this.txt_cardNumber.TabIndex = 13;
            // 
            // lbl_cardNumber
            // 
            this.lbl_cardNumber.AutoSize = true;
            this.lbl_cardNumber.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_cardNumber.Location = new System.Drawing.Point(6, 87);
            this.lbl_cardNumber.Name = "lbl_cardNumber";
            this.lbl_cardNumber.Size = new System.Drawing.Size(67, 19);
            this.lbl_cardNumber.TabIndex = 11;
            this.lbl_cardNumber.Text = "Card No";
            // 
            // cmb_type
            // 
            this.cmb_type.FormattingEnabled = true;
            this.cmb_type.Location = new System.Drawing.Point(102, 43);
            this.cmb_type.Name = "cmb_type";
            this.cmb_type.Size = new System.Drawing.Size(427, 24);
            this.cmb_type.TabIndex = 10;
            // 
            // lbl_type
            // 
            this.lbl_type.AutoSize = true;
            this.lbl_type.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_type.Location = new System.Drawing.Point(6, 43);
            this.lbl_type.Name = "lbl_type";
            this.lbl_type.Size = new System.Drawing.Size(42, 19);
            this.lbl_type.TabIndex = 9;
            this.lbl_type.Text = "Type";
            // 
            // pcb_reloadGuestFolio
            // 
            this.pcb_reloadGuestFolio.BackColor = System.Drawing.Color.Silver;
            this.pcb_reloadGuestFolio.BackgroundImage = global::maxxis.Properties.Resources.reload;
            this.pcb_reloadGuestFolio.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pcb_reloadGuestFolio.Location = new System.Drawing.Point(496, 157);
            this.pcb_reloadGuestFolio.Name = "pcb_reloadGuestFolio";
            this.pcb_reloadGuestFolio.Size = new System.Drawing.Size(33, 31);
            this.pcb_reloadGuestFolio.TabIndex = 23;
            this.pcb_reloadGuestFolio.TabStop = false;
            // 
            // pcb_searchFolioNumber
            // 
            this.pcb_searchFolioNumber.BackColor = System.Drawing.Color.Silver;
            this.pcb_searchFolioNumber.BackgroundImage = global::maxxis.Properties.Resources.search;
            this.pcb_searchFolioNumber.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pcb_searchFolioNumber.Location = new System.Drawing.Point(451, 157);
            this.pcb_searchFolioNumber.Name = "pcb_searchFolioNumber";
            this.pcb_searchFolioNumber.Size = new System.Drawing.Size(33, 31);
            this.pcb_searchFolioNumber.TabIndex = 22;
            this.pcb_searchFolioNumber.TabStop = false;
            // 
            // pcb_addBillTo
            // 
            this.pcb_addBillTo.BackColor = System.Drawing.Color.Silver;
            this.pcb_addBillTo.BackgroundImage = global::maxxis.Properties.Resources.add;
            this.pcb_addBillTo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pcb_addBillTo.Location = new System.Drawing.Point(496, 116);
            this.pcb_addBillTo.Name = "pcb_addBillTo";
            this.pcb_addBillTo.Size = new System.Drawing.Size(33, 31);
            this.pcb_addBillTo.TabIndex = 19;
            this.pcb_addBillTo.TabStop = false;
            // 
            // pcb_searchBillTo
            // 
            this.pcb_searchBillTo.BackColor = System.Drawing.Color.Silver;
            this.pcb_searchBillTo.BackgroundImage = global::maxxis.Properties.Resources.search;
            this.pcb_searchBillTo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pcb_searchBillTo.Location = new System.Drawing.Point(451, 116);
            this.pcb_searchBillTo.Name = "pcb_searchBillTo";
            this.pcb_searchBillTo.Size = new System.Drawing.Size(33, 31);
            this.pcb_searchBillTo.TabIndex = 18;
            this.pcb_searchBillTo.TabStop = false;
            // 
            // tab_visaInformation
            // 
            this.tab_visaInformation.BackColor = System.Drawing.Color.Silver;
            this.tab_visaInformation.Controls.Add(this.pnl_visaInfoTwo);
            this.tab_visaInformation.Controls.Add(this.pnl_visaInfoOne);
            this.tab_visaInformation.Location = new System.Drawing.Point(4, 38);
            this.tab_visaInformation.Name = "tab_visaInformation";
            this.tab_visaInformation.Padding = new System.Windows.Forms.Padding(3);
            this.tab_visaInformation.Size = new System.Drawing.Size(633, 373);
            this.tab_visaInformation.TabIndex = 2;
            this.tab_visaInformation.Text = "Visa Information";
            // 
            // pnl_visaInfoTwo
            // 
            this.pnl_visaInfoTwo.AutoScroll = true;
            this.pnl_visaInfoTwo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnl_visaInfoTwo.Controls.Add(this.txt_purposeOfVisit);
            this.pnl_visaInfoTwo.Controls.Add(this.dtp_timeOfArrival);
            this.pnl_visaInfoTwo.Controls.Add(this.txt_departureTransportation);
            this.pnl_visaInfoTwo.Controls.Add(this.lbl_dateOfArrival);
            this.pnl_visaInfoTwo.Controls.Add(this.txt_goingTo);
            this.pnl_visaInfoTwo.Controls.Add(this.lbl_timeOfArrival);
            this.pnl_visaInfoTwo.Controls.Add(this.lbl_purposeOfVisit);
            this.pnl_visaInfoTwo.Controls.Add(this.dtp_dateOfArrival);
            this.pnl_visaInfoTwo.Controls.Add(this.lbl_goingTo);
            this.pnl_visaInfoTwo.Controls.Add(this.lbl_departureTransportation);
            this.pnl_visaInfoTwo.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnl_visaInfoTwo.Location = new System.Drawing.Point(3, 157);
            this.pnl_visaInfoTwo.Name = "pnl_visaInfoTwo";
            this.pnl_visaInfoTwo.Size = new System.Drawing.Size(627, 217);
            this.pnl_visaInfoTwo.TabIndex = 44;
            // 
            // txt_purposeOfVisit
            // 
            this.txt_purposeOfVisit.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_purposeOfVisit.Location = new System.Drawing.Point(195, 66);
            this.txt_purposeOfVisit.Name = "txt_purposeOfVisit";
            this.txt_purposeOfVisit.Size = new System.Drawing.Size(299, 23);
            this.txt_purposeOfVisit.TabIndex = 44;
            // 
            // dtp_timeOfArrival
            // 
            this.dtp_timeOfArrival.CustomFormat = "  hh:mm:ss tt";
            this.dtp_timeOfArrival.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtp_timeOfArrival.Location = new System.Drawing.Point(195, 37);
            this.dtp_timeOfArrival.Name = "dtp_timeOfArrival";
            this.dtp_timeOfArrival.ShowCheckBox = true;
            this.dtp_timeOfArrival.ShowUpDown = true;
            this.dtp_timeOfArrival.Size = new System.Drawing.Size(148, 23);
            this.dtp_timeOfArrival.TabIndex = 43;
            // 
            // txt_departureTransportation
            // 
            this.txt_departureTransportation.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_departureTransportation.Location = new System.Drawing.Point(195, 124);
            this.txt_departureTransportation.Name = "txt_departureTransportation";
            this.txt_departureTransportation.Size = new System.Drawing.Size(299, 23);
            this.txt_departureTransportation.TabIndex = 42;
            // 
            // lbl_dateOfArrival
            // 
            this.lbl_dateOfArrival.AutoSize = true;
<<<<<<< HEAD
            this.lbl_dateOfArrival.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
<<<<<<< HEAD
=======
=======
            this.lbl_dateOfArrival.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
>>>>>>> 3931348c34a4e1b1de18778096fd19dc0e91573a
>>>>>>> 522a8075ec8125153832dce14daa61967b8a869e
            this.lbl_dateOfArrival.Location = new System.Drawing.Point(20, 11);
            this.lbl_dateOfArrival.Name = "lbl_dateOfArrival";
            this.lbl_dateOfArrival.Size = new System.Drawing.Size(103, 19);
            this.lbl_dateOfArrival.TabIndex = 26;
            this.lbl_dateOfArrival.Text = "Date of Arrival";
            // 
            // txt_goingTo
            // 
            this.txt_goingTo.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_goingTo.Location = new System.Drawing.Point(195, 95);
            this.txt_goingTo.Name = "txt_goingTo";
            this.txt_goingTo.Size = new System.Drawing.Size(299, 23);
            this.txt_goingTo.TabIndex = 41;
            // 
            // lbl_timeOfArrival
            // 
            this.lbl_timeOfArrival.AutoSize = true;
<<<<<<< HEAD
            this.lbl_timeOfArrival.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
<<<<<<< HEAD
=======
=======
            this.lbl_timeOfArrival.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
>>>>>>> 3931348c34a4e1b1de18778096fd19dc0e91573a
>>>>>>> 522a8075ec8125153832dce14daa61967b8a869e
            this.lbl_timeOfArrival.Location = new System.Drawing.Point(20, 40);
            this.lbl_timeOfArrival.Name = "lbl_timeOfArrival";
            this.lbl_timeOfArrival.Size = new System.Drawing.Size(103, 19);
            this.lbl_timeOfArrival.TabIndex = 27;
            this.lbl_timeOfArrival.Text = "Time of Arrival";
            // 
            // lbl_purposeOfVisit
            // 
            this.lbl_purposeOfVisit.AutoSize = true;
<<<<<<< HEAD
            this.lbl_purposeOfVisit.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
<<<<<<< HEAD
=======
=======
            this.lbl_purposeOfVisit.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
>>>>>>> 3931348c34a4e1b1de18778096fd19dc0e91573a
>>>>>>> 522a8075ec8125153832dce14daa61967b8a869e
            this.lbl_purposeOfVisit.Location = new System.Drawing.Point(20, 69);
            this.lbl_purposeOfVisit.Name = "lbl_purposeOfVisit";
            this.lbl_purposeOfVisit.Size = new System.Drawing.Size(116, 19);
            this.lbl_purposeOfVisit.TabIndex = 28;
            this.lbl_purposeOfVisit.Text = "Purpose of Visit";
            // 
            // dtp_dateOfArrival
            // 
            this.dtp_dateOfArrival.CalendarTitleBackColor = System.Drawing.SystemColors.ControlText;
            this.dtp_dateOfArrival.CalendarTitleForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.dtp_dateOfArrival.CustomFormat = "  dd/MM/yyyy     - dddd";
            this.dtp_dateOfArrival.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtp_dateOfArrival.Location = new System.Drawing.Point(195, 8);
            this.dtp_dateOfArrival.Name = "dtp_dateOfArrival";
            this.dtp_dateOfArrival.ShowCheckBox = true;
            this.dtp_dateOfArrival.Size = new System.Drawing.Size(207, 23);
            this.dtp_dateOfArrival.TabIndex = 39;
            // 
            // lbl_goingTo
            // 
            this.lbl_goingTo.AutoSize = true;
<<<<<<< HEAD
            this.lbl_goingTo.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
<<<<<<< HEAD
=======
=======
            this.lbl_goingTo.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
>>>>>>> 3931348c34a4e1b1de18778096fd19dc0e91573a
>>>>>>> 522a8075ec8125153832dce14daa61967b8a869e
            this.lbl_goingTo.Location = new System.Drawing.Point(20, 98);
            this.lbl_goingTo.Name = "lbl_goingTo";
            this.lbl_goingTo.Size = new System.Drawing.Size(69, 19);
            this.lbl_goingTo.TabIndex = 29;
            this.lbl_goingTo.Text = "Going to";
            // 
            // lbl_departureTransportation
            // 
            this.lbl_departureTransportation.AutoSize = true;
<<<<<<< HEAD
            this.lbl_departureTransportation.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
<<<<<<< HEAD
=======
=======
            this.lbl_departureTransportation.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
>>>>>>> 3931348c34a4e1b1de18778096fd19dc0e91573a
>>>>>>> 522a8075ec8125153832dce14daa61967b8a869e
            this.lbl_departureTransportation.Location = new System.Drawing.Point(20, 127);
            this.lbl_departureTransportation.Name = "lbl_departureTransportation";
            this.lbl_departureTransportation.Size = new System.Drawing.Size(162, 19);
            this.lbl_departureTransportation.TabIndex = 30;
            this.lbl_departureTransportation.Text = "Depart. Transportation";
            // 
            // pnl_visaInfoOne
            // 
            this.pnl_visaInfoOne.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnl_visaInfoOne.Controls.Add(this.txt_arrivedForm);
            this.pnl_visaInfoOne.Controls.Add(this.lbl_serialNumber);
            this.pnl_visaInfoOne.Controls.Add(this.txt_visaIssuePlace);
            this.pnl_visaInfoOne.Controls.Add(this.lbl_visaNumber);
            this.pnl_visaInfoOne.Controls.Add(this.txt_visaNumber);
            this.pnl_visaInfoOne.Controls.Add(this.lbl_visaDate);
            this.pnl_visaInfoOne.Controls.Add(this.dtp_visaDate);
            this.pnl_visaInfoOne.Controls.Add(this.lbl_vissaIssuePlace);
            this.pnl_visaInfoOne.Controls.Add(this.txt_serialNumber);
            this.pnl_visaInfoOne.Controls.Add(this.lbl_arrivedForm);
            this.pnl_visaInfoOne.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnl_visaInfoOne.Location = new System.Drawing.Point(3, 3);
            this.pnl_visaInfoOne.Name = "pnl_visaInfoOne";
            this.pnl_visaInfoOne.Size = new System.Drawing.Size(627, 154);
            this.pnl_visaInfoOne.TabIndex = 43;
            // 
            // txt_arrivedForm
            // 
            this.txt_arrivedForm.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_arrivedForm.Location = new System.Drawing.Point(195, 119);
            this.txt_arrivedForm.Name = "txt_arrivedForm";
            this.txt_arrivedForm.Size = new System.Drawing.Size(299, 23);
            this.txt_arrivedForm.TabIndex = 42;
            // 
            // lbl_serialNumber
            // 
            this.lbl_serialNumber.AutoSize = true;
            this.lbl_serialNumber.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_serialNumber.Location = new System.Drawing.Point(20, 7);
            this.lbl_serialNumber.Name = "lbl_serialNumber";
            this.lbl_serialNumber.Size = new System.Drawing.Size(71, 19);
            this.lbl_serialNumber.TabIndex = 26;
            this.lbl_serialNumber.Text = "Serial No";
            // 
            // txt_visaIssuePlace
            // 
            this.txt_visaIssuePlace.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_visaIssuePlace.Location = new System.Drawing.Point(195, 90);
            this.txt_visaIssuePlace.Name = "txt_visaIssuePlace";
            this.txt_visaIssuePlace.Size = new System.Drawing.Size(299, 23);
            this.txt_visaIssuePlace.TabIndex = 41;
            // 
            // lbl_visaNumber
            // 
            this.lbl_visaNumber.AutoSize = true;
            this.lbl_visaNumber.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_visaNumber.Location = new System.Drawing.Point(20, 36);
            this.lbl_visaNumber.Name = "lbl_visaNumber";
            this.lbl_visaNumber.Size = new System.Drawing.Size(62, 19);
            this.lbl_visaNumber.TabIndex = 27;
            this.lbl_visaNumber.Text = "Visa No";
            // 
            // txt_visaNumber
            // 
            this.txt_visaNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_visaNumber.Location = new System.Drawing.Point(195, 32);
            this.txt_visaNumber.Name = "txt_visaNumber";
            this.txt_visaNumber.Size = new System.Drawing.Size(299, 23);
            this.txt_visaNumber.TabIndex = 40;
            // 
            // lbl_visaDate
            // 
            this.lbl_visaDate.AutoSize = true;
            this.lbl_visaDate.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_visaDate.Location = new System.Drawing.Point(20, 65);
            this.lbl_visaDate.Name = "lbl_visaDate";
            this.lbl_visaDate.Size = new System.Drawing.Size(74, 19);
            this.lbl_visaDate.TabIndex = 28;
            this.lbl_visaDate.Text = "Visa Date";
            // 
            // dtp_visaDate
            // 
            this.dtp_visaDate.CalendarTitleBackColor = System.Drawing.SystemColors.ControlText;
            this.dtp_visaDate.CalendarTitleForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.dtp_visaDate.CustomFormat = "  dd/MM/yyyy     - dddd";
            this.dtp_visaDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtp_visaDate.Location = new System.Drawing.Point(195, 61);
            this.dtp_visaDate.Name = "dtp_visaDate";
            this.dtp_visaDate.ShowCheckBox = true;
            this.dtp_visaDate.Size = new System.Drawing.Size(207, 23);
            this.dtp_visaDate.TabIndex = 39;
            // 
            // lbl_vissaIssuePlace
            // 
            this.lbl_vissaIssuePlace.AutoSize = true;
            this.lbl_vissaIssuePlace.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_vissaIssuePlace.Location = new System.Drawing.Point(20, 94);
            this.lbl_vissaIssuePlace.Name = "lbl_vissaIssuePlace";
            this.lbl_vissaIssuePlace.Size = new System.Drawing.Size(127, 19);
            this.lbl_vissaIssuePlace.TabIndex = 29;
            this.lbl_vissaIssuePlace.Text = "Vissa Issue Place";
            // 
            // txt_serialNumber
            // 
            this.txt_serialNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_serialNumber.Location = new System.Drawing.Point(195, 3);
            this.txt_serialNumber.Name = "txt_serialNumber";
            this.txt_serialNumber.Size = new System.Drawing.Size(299, 23);
            this.txt_serialNumber.TabIndex = 31;
            // 
            // lbl_arrivedForm
            // 
            this.lbl_arrivedForm.AutoSize = true;
            this.lbl_arrivedForm.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_arrivedForm.Location = new System.Drawing.Point(20, 123);
            this.lbl_arrivedForm.Name = "lbl_arrivedForm";
            this.lbl_arrivedForm.Size = new System.Drawing.Size(95, 19);
            this.lbl_arrivedForm.TabIndex = 30;
            this.lbl_arrivedForm.Text = "Arrived Form";
            // 
<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> 522a8075ec8125153832dce14daa61967b8a869e
            // dgv_billingSummary
            // 
            this.dgv_billingSummary.AllowUserToAddRows = false;
            this.dgv_billingSummary.AllowUserToDeleteRows = false;
            this.dgv_billingSummary.BackgroundColor = System.Drawing.Color.Silver;
            this.dgv_billingSummary.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgv_billingSummary.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgv_billingSummary.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_billingSummary.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgv_billingSummary.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_billingSummary.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.col_Date,
            this.col_Particulars,
            this.col_Qty,
            this.col_Unit,
            this.col_Rate_Cost,
            this.col_TotalAmount});
            this.dgv_billingSummary.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv_billingSummary.EnableHeadersVisualStyles = false;
            this.dgv_billingSummary.Location = new System.Drawing.Point(3, 3);
            this.dgv_billingSummary.Name = "dgv_billingSummary";
            this.dgv_billingSummary.ReadOnly = true;
            this.dgv_billingSummary.RowHeadersVisible = false;
            this.dgv_billingSummary.Size = new System.Drawing.Size(627, 277);
            this.dgv_billingSummary.TabIndex = 42;
            // 
            // col_Date
            // 
            this.col_Date.HeaderText = "Date";
            this.col_Date.Name = "col_Date";
            this.col_Date.ReadOnly = true;
            // 
            // col_Particulars
            // 
            this.col_Particulars.HeaderText = "Particulars";
            this.col_Particulars.Name = "col_Particulars";
            this.col_Particulars.ReadOnly = true;
            this.col_Particulars.Width = 250;
            // 
            // col_Qty
            // 
            this.col_Qty.HeaderText = "Qty";
            this.col_Qty.Name = "col_Qty";
            this.col_Qty.ReadOnly = true;
            this.col_Qty.Width = 80;
            // 
            // col_Unit
            // 
            this.col_Unit.HeaderText = "Unit";
            this.col_Unit.Name = "col_Unit";
            this.col_Unit.ReadOnly = true;
            // 
            // col_Rate_Cost
            // 
            this.col_Rate_Cost.HeaderText = "Rate/Cost";
            this.col_Rate_Cost.Name = "col_Rate_Cost";
            this.col_Rate_Cost.ReadOnly = true;
            this.col_Rate_Cost.Width = 150;
            // 
            // col_TotalAmount
            // 
            this.col_TotalAmount.HeaderText = "Amount";
            this.col_TotalAmount.Name = "col_TotalAmount";
            this.col_TotalAmount.ReadOnly = true;
            this.col_TotalAmount.Width = 150;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(1263, 825);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 17);
            this.label1.TabIndex = 76;
<<<<<<< HEAD
=======
=======
            // dgv_listOfRooms
            // 
            this.dgv_listOfRooms.AllowUserToAddRows = false;
            this.dgv_listOfRooms.AllowUserToDeleteRows = false;
            this.dgv_listOfRooms.BackgroundColor = System.Drawing.Color.White;
            this.dgv_listOfRooms.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.DimGray;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Padding = new System.Windows.Forms.Padding(0, 7, 0, 7);
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.ControlLight;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_listOfRooms.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv_listOfRooms.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_listOfRooms.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.col_RoomType,
            this.col_Available,
            this.col_Quantity,
            this.col_RoomRate,
            this.col_Adult,
            this.col_Child,
            this.col_Amount});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv_listOfRooms.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgv_listOfRooms.EnableHeadersVisualStyles = false;
            this.dgv_listOfRooms.Location = new System.Drawing.Point(0, 0);
            this.dgv_listOfRooms.Name = "dgv_listOfRooms";
            this.dgv_listOfRooms.ReadOnly = true;
            this.dgv_listOfRooms.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_listOfRooms.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgv_listOfRooms.RowHeadersVisible = false;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.White;
            this.dgv_listOfRooms.RowsDefaultCellStyle = dataGridViewCellStyle4;
            this.dgv_listOfRooms.Size = new System.Drawing.Size(633, 172);
            this.dgv_listOfRooms.TabIndex = 48;
            // 
            // col_RoomType
            // 
            this.col_RoomType.HeaderText = "Room Type";
            this.col_RoomType.Name = "col_RoomType";
            this.col_RoomType.ReadOnly = true;
            this.col_RoomType.Width = 200;
            // 
            // col_Available
            // 
            this.col_Available.HeaderText = "Available";
            this.col_Available.Name = "col_Available";
            this.col_Available.ReadOnly = true;
            this.col_Available.Width = 80;
            // 
            // col_Quantity
            // 
            this.col_Quantity.HeaderText = "Qty.";
            this.col_Quantity.Name = "col_Quantity";
            this.col_Quantity.ReadOnly = true;
            this.col_Quantity.Width = 50;
            // 
            // col_RoomRate
            // 
            this.col_RoomRate.HeaderText = "Room Rate";
            this.col_RoomRate.Name = "col_RoomRate";
            this.col_RoomRate.ReadOnly = true;
            this.col_RoomRate.Width = 150;
            // 
            // col_Adult
            // 
            this.col_Adult.HeaderText = "Adult";
            this.col_Adult.Name = "col_Adult";
            this.col_Adult.ReadOnly = true;
            this.col_Adult.Width = 70;
            // 
            // col_Child
            // 
            this.col_Child.HeaderText = "Child";
            this.col_Child.Name = "col_Child";
            this.col_Child.ReadOnly = true;
            this.col_Child.Width = 70;
            // 
            // col_Amount
            // 
            this.col_Amount.HeaderText = "Amount";
            this.col_Amount.Name = "col_Amount";
            this.col_Amount.ReadOnly = true;
>>>>>>> 3931348c34a4e1b1de18778096fd19dc0e91573a
>>>>>>> 522a8075ec8125153832dce14daa61967b8a869e
            // 
            // Guest_Folio
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
<<<<<<< HEAD
            this.ClientSize = new System.Drawing.Size(1300, 730);
=======
<<<<<<< HEAD
            this.ClientSize = new System.Drawing.Size(1300, 730);
=======
            this.ClientSize = new System.Drawing.Size(666, 730);
>>>>>>> 3931348c34a4e1b1de18778096fd19dc0e91573a
>>>>>>> 522a8075ec8125153832dce14daa61967b8a869e
            this.Controls.Add(this.pnl_guestFolio);
            this.Controls.Add(this.guestFolio_panel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Guest_Folio";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Guest_Folio";
            this.guestFolio_panel.ResumeLayout(false);
            this.guestFolio_panel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pcb_guestFolio)).EndInit();
            this.pnl_guestFolio.ResumeLayout(false);
            this.pnl_guestFolio.PerformLayout();
            this.tabControl2.ResumeLayout(false);
            this.tab_guestRemarks.ResumeLayout(false);
            this.pnl_guestRemarks.ResumeLayout(false);
            this.pnl_guestRemarks.PerformLayout();
            this.tab_billingSummary.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tab_guestInformation.ResumeLayout(false);
            this.tab_guestInformation.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pcb_search)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcb_info)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcb_plusButton)).EndInit();
            this.tab_settlementOptions.ResumeLayout(false);
            this.tab_settlementOptions.PerformLayout();
            this.pnl_stayInformation.ResumeLayout(false);
            this.pnl_stayInformation.PerformLayout();
            this.stayInformation_panel.ResumeLayout(false);
            this.stayInformation_panel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pcb_reloadGuestFolio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcb_searchFolioNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcb_addBillTo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcb_searchBillTo)).EndInit();
            this.tab_visaInformation.ResumeLayout(false);
            this.pnl_visaInfoTwo.ResumeLayout(false);
            this.pnl_visaInfoTwo.PerformLayout();
            this.pnl_visaInfoOne.ResumeLayout(false);
            this.pnl_visaInfoOne.PerformLayout();
<<<<<<< HEAD
            ((System.ComponentModel.ISupportInitialize)(this.dgv_billingSummary)).EndInit();
=======
<<<<<<< HEAD
            ((System.ComponentModel.ISupportInitialize)(this.dgv_billingSummary)).EndInit();
=======
            ((System.ComponentModel.ISupportInitialize)(this.dgv_listOfRooms)).EndInit();
>>>>>>> 3931348c34a4e1b1de18778096fd19dc0e91573a
>>>>>>> 522a8075ec8125153832dce14daa61967b8a869e
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel guestFolio_panel;
        private System.Windows.Forms.Label lbl_guestFolio;
        private System.Windows.Forms.PictureBox pcb_guestFolio;
        private System.Windows.Forms.Panel pnl_guestFolio;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tab_settlementOptions;
        private System.Windows.Forms.Label lbl_type;
        private System.Windows.Forms.TextBox txt_expire;
        private System.Windows.Forms.Label lbl_expire;
        private System.Windows.Forms.TextBox txt_cardNumber;
        private System.Windows.Forms.Label lbl_cardNumber;
        private System.Windows.Forms.ComboBox cmb_type;
        private System.Windows.Forms.PictureBox pcb_addBillTo;
        private System.Windows.Forms.TextBox txt_billTo;
        private System.Windows.Forms.Label lbl_billTo;
        private System.Windows.Forms.PictureBox pcb_reloadGuestFolio;
        private System.Windows.Forms.PictureBox pcb_searchFolioNumber;
        private System.Windows.Forms.TextBox txt_folioNumber;
        private System.Windows.Forms.Label lbl_folioNumber;
        private System.Windows.Forms.PictureBox pcb_searchBillTo;
        private System.Windows.Forms.Panel pnl_stayInformation;
        private System.Windows.Forms.DomainUpDown dom_numberOfNights;
        private System.Windows.Forms.Label lbl_child;
        private System.Windows.Forms.Label lbl_adults;
        private System.Windows.Forms.Label lbl_numberOfNights;
        private System.Windows.Forms.Label lbl_departure;
        private System.Windows.Forms.Label lbl_arrival;
        private System.Windows.Forms.Panel stayInformation_panel;
        private System.Windows.Forms.Label lbl_stayInformation;
        private System.Windows.Forms.DomainUpDown dom_child;
        private System.Windows.Forms.DomainUpDown dom_adults;
        private System.Windows.Forms.DateTimePicker dtp_departureDate;
        private System.Windows.Forms.DateTimePicker dtp_arrivalDate;
        private System.Windows.Forms.DateTimePicker dtp_departureTime;
        private System.Windows.Forms.DateTimePicker dtp_arrivalTime;
        private System.Windows.Forms.TabPage tab_visaInformation;
        private System.Windows.Forms.Button btn_GoBack;
        private System.Windows.Forms.TextBox txt_serialNumber;
        private System.Windows.Forms.Label lbl_arrivedForm;
        private System.Windows.Forms.Label lbl_vissaIssuePlace;
        private System.Windows.Forms.Label lbl_visaDate;
        private System.Windows.Forms.Label lbl_visaNumber;
        private System.Windows.Forms.Label lbl_serialNumber;
        private System.Windows.Forms.DateTimePicker dtp_visaDate;
        private System.Windows.Forms.TextBox txt_arrivedForm;
        private System.Windows.Forms.TextBox txt_visaIssuePlace;
        private System.Windows.Forms.TextBox txt_visaNumber;
        private System.Windows.Forms.Panel pnl_visaInfoTwo;
        private System.Windows.Forms.TextBox txt_departureTransportation;
        private System.Windows.Forms.Label lbl_dateOfArrival;
        private System.Windows.Forms.TextBox txt_goingTo;
        private System.Windows.Forms.Label lbl_timeOfArrival;
        private System.Windows.Forms.Label lbl_purposeOfVisit;
        private System.Windows.Forms.DateTimePicker dtp_dateOfArrival;
        private System.Windows.Forms.Label lbl_goingTo;
        private System.Windows.Forms.Label lbl_departureTransportation;
        private System.Windows.Forms.Panel pnl_visaInfoOne;
        private System.Windows.Forms.TextBox txt_purposeOfVisit;
        private System.Windows.Forms.DateTimePicker dtp_timeOfArrival;
        private System.Windows.Forms.TabPage tab_guestInformation;
        private System.Windows.Forms.Label lbl_last_name;
        private System.Windows.Forms.Label lbl_showRates;
        private System.Windows.Forms.PictureBox pcb_search;
        private System.Windows.Forms.PictureBox pcb_info;
        private System.Windows.Forms.PictureBox pcb_plusButton;
        private System.Windows.Forms.TextBox txt_firstname;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label lbl_firstname;
        private System.Windows.Forms.TextBox txt_lastname;
        private System.Windows.Forms.Button btn_checkOut;
        private System.Windows.Forms.Label lbl_email;
        private System.Windows.Forms.TextBox txt_email;
        private System.Windows.Forms.Label lbl_contactDetails;
        private System.Windows.Forms.TextBox txt_contactDetails;
        private System.Windows.Forms.Label lbl_title;
        private System.Windows.Forms.TextBox txt_middleName;
        private System.Windows.Forms.Label lbl_middleName;
        private System.Windows.Forms.Label lbl_province;
        private System.Windows.Forms.TextBox txt_province;
        private System.Windows.Forms.Label lbl_city;
        private System.Windows.Forms.TextBox txt_city;
        private System.Windows.Forms.Label lbl_address;
        private System.Windows.Forms.TextBox txt_address;
        private System.Windows.Forms.Label lbl_dateOfBirth;
        private System.Windows.Forms.DateTimePicker dtb_dateOfBirth;
        private System.Windows.Forms.ComboBox cmb_homeCountry;
        private System.Windows.Forms.Label lbl_homeCountry;
        private System.Windows.Forms.Label lbl_companyAgency;
        private System.Windows.Forms.TextBox txt_companyAgency;
        private System.Windows.Forms.Label lbl_modeOfPayment;
        private System.Windows.Forms.RadioButton rbtn_partialCredit;
        private System.Windows.Forms.RadioButton rbtn_credit;
        private System.Windows.Forms.RadioButton rbtn_cash;
        private System.Windows.Forms.TabControl tabControl2;
        private System.Windows.Forms.TabPage tab_guestRemarks;
        private System.Windows.Forms.Panel pnl_guestRemarks;
        private System.Windows.Forms.TextBox txt_guestRemarks;
        private System.Windows.Forms.TabPage tab_billingSummary;
<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> 522a8075ec8125153832dce14daa61967b8a869e
        private System.Windows.Forms.DataGridView dgv_billingSummary;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_Date;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_Particulars;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_Qty;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_Unit;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_Rate_Cost;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_TotalAmount;
        private System.Windows.Forms.Label label1;
<<<<<<< HEAD
=======
=======
        private System.Windows.Forms.DataGridView dgv_listOfRooms;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_RoomType;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_Available;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_Quantity;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_RoomRate;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_Adult;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_Child;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_Amount;
>>>>>>> 3931348c34a4e1b1de18778096fd19dc0e91573a
>>>>>>> 522a8075ec8125153832dce14daa61967b8a869e
    }
}