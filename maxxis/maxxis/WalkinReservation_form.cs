﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace maxxis
{
    public partial class WalkinReservation_form : Form
    {
        //from guest arrival
        public string guest_check_in { get; set; }
        public int check_in_ready = 0;
        //variables
        string con = @"Data Source=LAPTOP-N5RT3GVU\SQLEXPRESS;Initial Catalog=DBMaxxis;Integrated Security=True";
        public WalkinReservation_form()
        {
            InitializeComponent();
        }
        private void WalkinReservation_form_Load(object sender, EventArgs e)
        {
            if (check_in_ready == 1)
            {
                checkInSet();
            }
            else
            {
                


                //changes the yellow selector
                pnl_newReservation_Selector.Height = btn_guestInformation.Height;
                pnl_newReservation_Selector.Top = btn_guestInformation.Top;

                //Relocates panel into one position
                pnl_guestInformation.Location = new Point(206, 0);
                pnl_accomodationDetails.Location = new Point(206, 0);
                pnl_hotelTransfer.Location = new Point(206, 0);
                pnl_businessSource.Location = new Point(206, 0);
                pnl_visaInformation.Location = new Point(206, 0);
                pnl_settlementOption.Location = new Point(206, 0);
                pnl_AD_information.Location = new Point(206, 0);
                pnl_billingSummary.Location = new Point(206, 0);
                pnl_discount.Location = new Point(206, 0);
                pnl_payments.Location = new Point(206, 0);
                pnl_folioSummary.Location = new Point(206, 0);

                //disables the panels
                pnl_guestInformation.Show();
                pnl_accomodationDetails.Hide();
                pnl_hotelTransfer.Hide();
                pnl_businessSource.Hide();
                pnl_visaInformation.Hide();
                pnl_settlementOption.Hide();
                pnl_AD_information.Hide();
                pnl_billingSummary.Hide();
                pnl_discount.Hide();
                pnl_payments.Hide();
                pnl_folioSummary.Hide();
            }
        }
        private void btn_reserve_Click(object sender, EventArgs e)
        {
            btn_add.Enabled = false;
            pnl_guestInformation.Enabled = true;
        }

        private void btn_cancel_Click(object sender, EventArgs e)
        {
            if (check_in_ready == 1)
            {
                this.Close();
            }
            else
            {
                this.Close();
            }

        }

        private void btn_save_Click(object sender, EventArgs e)
        {
            Add();
            pnl_guestInformation.Enabled = false;

        }
        private void btn_checkIn_Click(object sender, EventArgs e)
        {
            Checkin();
            this.Hide();
        }
        //Methods
        public void Add()
        {
            try
            {
                SqlConnection conDatabase = new SqlConnection(con);
                string Query = "insert into tbl_guestinfo(title,lastname,firstname,middlename,dateofbirth,address,contact_details,email,province,home_country,company_or_agency,city) values('"
                    + this.cmb_title.Text
                    + "','" + this.txt_lastName.Text
                    + "','" + this.txt_firstName.Text
                    + "','" + this.txt_middleName.Text
                    + "','" + this.dtb_dateOfBirth.Text
                    + "','" + this.txt_address.Text
                    + "','" + this.txt_contactDetails.Text
                    + "','" + this.txt_email.Text
                    + "','" + this.txt_province.Text
                    + "','" + this.cmb_homeCountry.Text
                    + "','" + this.txt_companyAgency.Text
                    + "','" + this.txt_city.Text + "');";
                SqlCommand MyCommand2 = new SqlCommand(Query, conDatabase);
                SqlDataReader MyReader2;
                conDatabase.Open();
                MessageBox.Show("Reservation Saved");
                MyReader2 = MyCommand2.ExecuteReader();
                while (MyReader2.Read())
                {
                }
                conDatabase.Close();
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void checkInSet()
        {
            SqlConnection conDatabase = new SqlConnection(con);
            SqlCommand cmdDataBase = new SqlCommand("SELECT * FROM tbl_guestinfo where id = '" + guest_check_in + "';", conDatabase);
            conDatabase.Open();
            SqlDataAdapter sda = new SqlDataAdapter();
            sda.SelectCommand = cmdDataBase;
            DataTable dt = new DataTable();
            sda.Fill(dt);
            try
            {
                foreach (DataRow item in dt.Rows)
                {
                    cmb_title.Text = item[1].ToString();
                    txt_lastName.Text = item[2].ToString();
                    txt_firstName.Text = item[3].ToString();
                    txt_middleName.Text = item[4].ToString();
                    dtb_dateOfBirth.Value = Convert.ToDateTime(item[5]);
                    txt_contactDetails.Text = item[6].ToString();
                    txt_address.Text = item[7].ToString();
                    txt_email.Text = item[8].ToString();
                    txt_province.Text = item[9].ToString();
                    cmb_homeCountry.Text = item[10].ToString();
                    txt_companyAgency.Text = item[11].ToString();
                    txt_city.Text = item[12].ToString();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            conDatabase.Close();
        }

        public void Checkin()
        {
            try
            {
                SqlConnection conDatabase = new SqlConnection(con);
                string Query = "update tbl_guestinfo set checked_in = ('" + "1" + "') where id = '" + this.guest_check_in  + "' ;";
                SqlCommand MyCommand2 = new SqlCommand(Query, conDatabase);
                SqlDataReader MyReader2;
                conDatabase.Open();
                MessageBox.Show("Guest Checked In");
                MyReader2 = MyCommand2.ExecuteReader();
                while (MyReader2.Read())
                {
                }
                conDatabase.Close();
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               

        private void btn_guestInformation_Click(object sender, EventArgs e)
        {
            //changes the yellow selector
            pnl_newReservation_Selector.Height = btn_guestInformation.Height;
            pnl_newReservation_Selector.Top = btn_guestInformation.Top;

            //disables the panels
            pnl_guestInformation.Show();
            pnl_accomodationDetails.Hide();
            pnl_hotelTransfer.Hide();
            pnl_businessSource.Hide();
            pnl_visaInformation.Hide();
            pnl_settlementOption.Hide();
            pnl_AD_information.Hide();
            pnl_billingSummary.Hide();
            pnl_discount.Hide();
            pnl_payments.Hide();
            pnl_folioSummary.Hide();
        }

        private void btn_accomodationDetails_Click(object sender, EventArgs e)
        {
            //changes the yellow selector
            pnl_newReservation_Selector.Height = btn_accomodationDetails.Height;
            pnl_newReservation_Selector.Top = btn_accomodationDetails.Top;


            //disables the panels
            pnl_guestInformation.Hide();
            pnl_accomodationDetails.Show();
            pnl_hotelTransfer.Hide();
            pnl_businessSource.Hide();
            pnl_visaInformation.Hide();
            pnl_settlementOption.Hide();
            pnl_AD_information.Hide();
            pnl_billingSummary.Hide();
            pnl_discount.Hide();
            pnl_payments.Hide();
            pnl_folioSummary.Hide();
        }

        private void btn_hotelTransfer_Click(object sender, EventArgs e)
        {
            //changes the yellow selector
            pnl_newReservation_Selector.Height = btn_hotelTransfer.Height;
            pnl_newReservation_Selector.Top = btn_hotelTransfer.Top;

            //disables the panels
            pnl_guestInformation.Hide();
            pnl_accomodationDetails.Hide();
            pnl_hotelTransfer.Show();
            pnl_businessSource.Hide();
            pnl_visaInformation.Hide();
            pnl_settlementOption.Hide();
            pnl_AD_information.Hide();
            pnl_billingSummary.Hide();
            pnl_discount.Hide();
            pnl_payments.Hide();
            pnl_folioSummary.Hide();
        }

        private void btn_businessSource_Click(object sender, EventArgs e)
        {
            //changes the yellow selector
            pnl_newReservation_Selector.Height = btn_businessSource.Height;
            pnl_newReservation_Selector.Top = btn_businessSource.Top;

            //disables the panels
            pnl_guestInformation.Hide();
            pnl_accomodationDetails.Hide();
            pnl_hotelTransfer.Hide();
            pnl_businessSource.Show();
            pnl_visaInformation.Hide();
            pnl_settlementOption.Hide();
            pnl_AD_information.Hide();
            pnl_billingSummary.Hide();
            pnl_discount.Hide();
            pnl_payments.Hide();
            pnl_folioSummary.Hide();
        }

        private void btn_visaInformation_Click(object sender, EventArgs e)
        {
            //changes the yellow selector
            pnl_newReservation_Selector.Height = btn_visaInformation.Height;
            pnl_newReservation_Selector.Top = btn_visaInformation.Top;

            //disables the panels
            pnl_guestInformation.Hide();
            pnl_accomodationDetails.Hide();
            pnl_hotelTransfer.Hide();
            pnl_businessSource.Hide();
            pnl_visaInformation.Show();
            pnl_settlementOption.Hide();
            pnl_AD_information.Hide();
            pnl_billingSummary.Hide();
            pnl_discount.Hide();
            pnl_payments.Hide();
            pnl_folioSummary.Hide();
        }

        private void btn_settlementOption_Click(object sender, EventArgs e)
        {
            //changes the yellow selector
            pnl_newReservation_Selector.Height = btn_settlementOption.Height;
            pnl_newReservation_Selector.Top = btn_settlementOption.Top;

            //disables the panels
            pnl_guestInformation.Hide();
            pnl_accomodationDetails.Hide();
            pnl_hotelTransfer.Hide();
            pnl_businessSource.Hide();
            pnl_visaInformation.Hide();
            pnl_settlementOption.Show();
            pnl_AD_information.Hide();
            pnl_billingSummary.Hide();
            pnl_discount.Hide();
            pnl_payments.Hide();
            pnl_folioSummary.Hide();
        }

        private void btn_billingSummary_Click(object sender, EventArgs e)
        {
            //changes the yellow selector
            pnl_newReservation_Selector.Height = btn_billingSummary.Height;
            pnl_newReservation_Selector.Top = btn_billingSummary.Top;

            //disables the panels
            pnl_guestInformation.Hide();
            pnl_accomodationDetails.Hide();
            pnl_hotelTransfer.Hide();
            pnl_businessSource.Hide();
            pnl_visaInformation.Hide();
            pnl_settlementOption.Hide();
            pnl_AD_information.Hide();
            pnl_billingSummary.Show();
            pnl_discount.Hide();
            pnl_payments.Hide();
            pnl_folioSummary.Hide();
        }

        private void btn_discounts_Click(object sender, EventArgs e)
        {
            //changes the yellow selector
            pnl_newReservation_Selector.Height = btn_discounts.Height;
            pnl_newReservation_Selector.Top = btn_discounts.Top;

            //disables the panels
            pnl_guestInformation.Hide();
            pnl_accomodationDetails.Hide();
            pnl_hotelTransfer.Hide();
            pnl_businessSource.Hide();
            pnl_visaInformation.Hide();
            pnl_settlementOption.Hide();
            pnl_AD_information.Hide();
            pnl_billingSummary.Hide();
            pnl_discount.Show();
            pnl_payments.Hide();
            pnl_folioSummary.Hide();
        }

        private void btn_payments_Click(object sender, EventArgs e)
        {
            //changes the yellow selector
            pnl_newReservation_Selector.Height = btn_payments.Height;
            pnl_newReservation_Selector.Top = btn_payments.Top;

            //disables the panels
            pnl_guestInformation.Hide();
            pnl_accomodationDetails.Hide();
            pnl_hotelTransfer.Hide();
            pnl_businessSource.Hide();
            pnl_visaInformation.Hide();
            pnl_settlementOption.Hide();
            pnl_AD_information.Hide();
            pnl_billingSummary.Hide();
            pnl_discount.Hide();
            pnl_payments.Show();
            pnl_folioSummary.Hide();
        }

        private void btn_folioSummary_Click(object sender, EventArgs e)
        {
            //changes the yellow selector
            pnl_newReservation_Selector.Height = btn_folioSummary.Height;
            pnl_newReservation_Selector.Top = btn_folioSummary.Top;

            //disables the panels
            pnl_guestInformation.Hide();
            pnl_accomodationDetails.Hide();
            pnl_hotelTransfer.Hide();
            pnl_businessSource.Hide();
            pnl_visaInformation.Hide();
            pnl_settlementOption.Hide();
            pnl_AD_information.Hide();
            pnl_billingSummary.Hide();
            pnl_discount.Hide();
            pnl_payments.Hide();
            pnl_folioSummary.Show();
        }

        private void btn_arrivalAndDeparture_Click(object sender, EventArgs e)
        {
            //changes the yellow selector
            pnl_newReservation_Selector.Height = btn_arrivalAndDeparture.Height;
            pnl_newReservation_Selector.Top = btn_arrivalAndDeparture.Top;

            //disables the panels
            pnl_guestInformation.Hide();
            pnl_accomodationDetails.Hide();
            pnl_hotelTransfer.Hide();
            pnl_businessSource.Hide();
            pnl_visaInformation.Hide();
            pnl_settlementOption.Hide();
            pnl_AD_information.Show();
            pnl_billingSummary.Hide();
            pnl_discount.Hide();
            pnl_payments.Hide();
            pnl_folioSummary.Hide();
        }
    }
    
}
