﻿namespace maxxis
{
    partial class RateInformation_form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnl_rateInformation = new System.Windows.Forms.Panel();
            this.lbl_rateInformation = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lbl_firstName = new System.Windows.Forms.Label();
            this.lbl_lastName = new System.Windows.Forms.Label();
            this.lbl_folioNumber = new System.Windows.Forms.Label();
            this.lbl_checkIn = new System.Windows.Forms.Label();
            this.lbl_checkOut = new System.Windows.Forms.Label();
            this.txt_firstName = new System.Windows.Forms.TextBox();
            this.txt_lastName = new System.Windows.Forms.TextBox();
            this.txt_folioNumber = new System.Windows.Forms.TextBox();
            this.dtp_checkInTime = new System.Windows.Forms.DateTimePicker();
            this.dtp_checkInDate = new System.Windows.Forms.DateTimePicker();
            this.dtp_checkOutTime = new System.Windows.Forms.DateTimePicker();
            this.dtp_checkOutDate = new System.Windows.Forms.DateTimePicker();
            this.txt_roundOff = new System.Windows.Forms.TextBox();
            this.txt_totalRate = new System.Windows.Forms.TextBox();
            this.txt_totalCharges = new System.Windows.Forms.TextBox();
            this.txt_amountPaid = new System.Windows.Forms.TextBox();
            this.txt_discount = new System.Windows.Forms.TextBox();
            this.txt_totalTax = new System.Windows.Forms.TextBox();
            this.txt_total = new System.Windows.Forms.TextBox();
            this.txt_deposit = new System.Windows.Forms.TextBox();
            this.txt_extraCharges = new System.Windows.Forms.TextBox();
            this.txt_flatDiscount = new System.Windows.Forms.TextBox();
            this.txt_balance = new System.Windows.Forms.TextBox();
            this.lbl_extraCharges = new System.Windows.Forms.Label();
            this.lbl_balance = new System.Windows.Forms.Label();
            this.lbl_deposit = new System.Windows.Forms.Label();
            this.lbl_Total = new System.Windows.Forms.Label();
            this.lbl_totalTax = new System.Windows.Forms.Label();
            this.lbl_discount = new System.Windows.Forms.Label();
            this.lbl_amountPaid = new System.Windows.Forms.Label();
            this.lbl_totalRate = new System.Windows.Forms.Label();
            this.lbl_roundOff = new System.Windows.Forms.Label();
            this.lbl_flatDiscount = new System.Windows.Forms.Label();
            this.lbl_totalCharges = new System.Windows.Forms.Label();
            this.btn_GoBack = new System.Windows.Forms.Button();
            this.pnl_rateInformation.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnl_rateInformation
            // 
            this.pnl_rateInformation.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.pnl_rateInformation.Controls.Add(this.btn_GoBack);
            this.pnl_rateInformation.Controls.Add(this.lbl_rateInformation);
            this.pnl_rateInformation.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnl_rateInformation.Location = new System.Drawing.Point(0, 0);
            this.pnl_rateInformation.Name = "pnl_rateInformation";
            this.pnl_rateInformation.Size = new System.Drawing.Size(489, 35);
            this.pnl_rateInformation.TabIndex = 97;
            // 
            // lbl_rateInformation
            // 
            this.lbl_rateInformation.AutoSize = true;
            this.lbl_rateInformation.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.lbl_rateInformation.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_rateInformation.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(126)))), ((int)(((byte)(57)))));
            this.lbl_rateInformation.Location = new System.Drawing.Point(4, 9);
            this.lbl_rateInformation.Name = "lbl_rateInformation";
            this.lbl_rateInformation.Size = new System.Drawing.Size(105, 16);
            this.lbl_rateInformation.TabIndex = 0;
            this.lbl_rateInformation.Text = "Rate Information";
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.dtp_checkOutTime);
            this.panel1.Controls.Add(this.dtp_checkOutDate);
            this.panel1.Controls.Add(this.dtp_checkInTime);
            this.panel1.Controls.Add(this.dtp_checkInDate);
            this.panel1.Controls.Add(this.txt_folioNumber);
            this.panel1.Controls.Add(this.txt_lastName);
            this.panel1.Controls.Add(this.txt_firstName);
            this.panel1.Controls.Add(this.lbl_checkOut);
            this.panel1.Controls.Add(this.lbl_checkIn);
            this.panel1.Controls.Add(this.lbl_folioNumber);
            this.panel1.Controls.Add(this.lbl_lastName);
            this.panel1.Controls.Add(this.lbl_firstName);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 35);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(245, 289);
            this.panel1.TabIndex = 98;
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.lbl_totalCharges);
            this.panel2.Controls.Add(this.txt_balance);
            this.panel2.Controls.Add(this.lbl_flatDiscount);
            this.panel2.Controls.Add(this.lbl_roundOff);
            this.panel2.Controls.Add(this.txt_flatDiscount);
            this.panel2.Controls.Add(this.lbl_totalRate);
            this.panel2.Controls.Add(this.txt_extraCharges);
            this.panel2.Controls.Add(this.txt_deposit);
            this.panel2.Controls.Add(this.txt_total);
            this.panel2.Controls.Add(this.txt_totalTax);
            this.panel2.Controls.Add(this.txt_discount);
            this.panel2.Controls.Add(this.lbl_amountPaid);
            this.panel2.Controls.Add(this.txt_amountPaid);
            this.panel2.Controls.Add(this.lbl_discount);
            this.panel2.Controls.Add(this.lbl_totalTax);
            this.panel2.Controls.Add(this.lbl_Total);
            this.panel2.Controls.Add(this.lbl_deposit);
            this.panel2.Controls.Add(this.txt_totalCharges);
            this.panel2.Controls.Add(this.lbl_balance);
            this.panel2.Controls.Add(this.txt_totalRate);
            this.panel2.Controls.Add(this.lbl_extraCharges);
            this.panel2.Controls.Add(this.txt_roundOff);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel2.Location = new System.Drawing.Point(245, 35);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(244, 289);
            this.panel2.TabIndex = 99;
            // 
            // lbl_firstName
            // 
            this.lbl_firstName.AutoSize = true;
            this.lbl_firstName.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_firstName.Location = new System.Drawing.Point(4, 14);
            this.lbl_firstName.Name = "lbl_firstName";
            this.lbl_firstName.Size = new System.Drawing.Size(73, 16);
            this.lbl_firstName.TabIndex = 76;
            this.lbl_firstName.Text = "First Name";
            // 
            // lbl_lastName
            // 
            this.lbl_lastName.AutoSize = true;
            this.lbl_lastName.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_lastName.Location = new System.Drawing.Point(4, 64);
            this.lbl_lastName.Name = "lbl_lastName";
            this.lbl_lastName.Size = new System.Drawing.Size(72, 16);
            this.lbl_lastName.TabIndex = 77;
            this.lbl_lastName.Text = "Last Name";
            // 
            // lbl_folioNumber
            // 
            this.lbl_folioNumber.AutoSize = true;
            this.lbl_folioNumber.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_folioNumber.Location = new System.Drawing.Point(4, 113);
            this.lbl_folioNumber.Name = "lbl_folioNumber";
            this.lbl_folioNumber.Size = new System.Drawing.Size(63, 16);
            this.lbl_folioNumber.TabIndex = 78;
            this.lbl_folioNumber.Text = "Folio No.";
            // 
            // lbl_checkIn
            // 
            this.lbl_checkIn.AutoSize = true;
            this.lbl_checkIn.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_checkIn.Location = new System.Drawing.Point(4, 162);
            this.lbl_checkIn.Name = "lbl_checkIn";
            this.lbl_checkIn.Size = new System.Drawing.Size(92, 16);
            this.lbl_checkIn.TabIndex = 79;
            this.lbl_checkIn.Text = "Check In Date";
            // 
            // lbl_checkOut
            // 
            this.lbl_checkOut.AutoSize = true;
            this.lbl_checkOut.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_checkOut.Location = new System.Drawing.Point(4, 212);
            this.lbl_checkOut.Name = "lbl_checkOut";
            this.lbl_checkOut.Size = new System.Drawing.Size(104, 16);
            this.lbl_checkOut.TabIndex = 80;
            this.lbl_checkOut.Text = "Check Out Date";
            // 
            // txt_firstName
            // 
            this.txt_firstName.Location = new System.Drawing.Point(7, 33);
            this.txt_firstName.Name = "txt_firstName";
            this.txt_firstName.Size = new System.Drawing.Size(214, 20);
            this.txt_firstName.TabIndex = 83;
            // 
            // txt_lastName
            // 
            this.txt_lastName.Location = new System.Drawing.Point(7, 83);
            this.txt_lastName.Name = "txt_lastName";
            this.txt_lastName.Size = new System.Drawing.Size(214, 20);
            this.txt_lastName.TabIndex = 84;
            // 
            // txt_folioNumber
            // 
            this.txt_folioNumber.Location = new System.Drawing.Point(7, 134);
            this.txt_folioNumber.Name = "txt_folioNumber";
            this.txt_folioNumber.Size = new System.Drawing.Size(214, 20);
            this.txt_folioNumber.TabIndex = 85;
            // 
            // dtp_checkInTime
            // 
            this.dtp_checkInTime.CustomFormat = "hh:mm:ss tt";
            this.dtp_checkInTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtp_checkInTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtp_checkInTime.Location = new System.Drawing.Point(120, 180);
            this.dtp_checkInTime.Name = "dtp_checkInTime";
            this.dtp_checkInTime.ShowUpDown = true;
            this.dtp_checkInTime.Size = new System.Drawing.Size(101, 23);
            this.dtp_checkInTime.TabIndex = 87;
            // 
            // dtp_checkInDate
            // 
            this.dtp_checkInDate.CalendarTitleBackColor = System.Drawing.SystemColors.ControlText;
            this.dtp_checkInDate.CalendarTitleForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.dtp_checkInDate.CustomFormat = "dd/MM/yyyy";
            this.dtp_checkInDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtp_checkInDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtp_checkInDate.Location = new System.Drawing.Point(7, 181);
            this.dtp_checkInDate.Name = "dtp_checkInDate";
            this.dtp_checkInDate.Size = new System.Drawing.Size(107, 23);
            this.dtp_checkInDate.TabIndex = 86;
            // 
            // dtp_checkOutTime
            // 
            this.dtp_checkOutTime.CustomFormat = "hh:mm:ss tt";
            this.dtp_checkOutTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtp_checkOutTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtp_checkOutTime.Location = new System.Drawing.Point(120, 228);
            this.dtp_checkOutTime.Name = "dtp_checkOutTime";
            this.dtp_checkOutTime.ShowUpDown = true;
            this.dtp_checkOutTime.Size = new System.Drawing.Size(101, 23);
            this.dtp_checkOutTime.TabIndex = 89;
            // 
            // dtp_checkOutDate
            // 
            this.dtp_checkOutDate.CalendarTitleBackColor = System.Drawing.SystemColors.ControlText;
            this.dtp_checkOutDate.CalendarTitleForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.dtp_checkOutDate.CustomFormat = "dd/MM/yyyy";
            this.dtp_checkOutDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtp_checkOutDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtp_checkOutDate.Location = new System.Drawing.Point(7, 229);
            this.dtp_checkOutDate.Name = "dtp_checkOutDate";
            this.dtp_checkOutDate.Size = new System.Drawing.Size(107, 23);
            this.dtp_checkOutDate.TabIndex = 88;
            // 
            // txt_roundOff
            // 
            this.txt_roundOff.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_roundOff.Location = new System.Drawing.Point(124, 230);
            this.txt_roundOff.Name = "txt_roundOff";
            this.txt_roundOff.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txt_roundOff.Size = new System.Drawing.Size(108, 22);
            this.txt_roundOff.TabIndex = 95;
            this.txt_roundOff.Text = "0.00";
            // 
            // txt_totalRate
            // 
            this.txt_totalRate.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_totalRate.Location = new System.Drawing.Point(124, 83);
            this.txt_totalRate.Name = "txt_totalRate";
            this.txt_totalRate.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txt_totalRate.Size = new System.Drawing.Size(108, 22);
            this.txt_totalRate.TabIndex = 89;
            this.txt_totalRate.Text = "0.00";
            // 
            // txt_totalCharges
            // 
            this.txt_totalCharges.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_totalCharges.Location = new System.Drawing.Point(124, 8);
            this.txt_totalCharges.Name = "txt_totalCharges";
            this.txt_totalCharges.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txt_totalCharges.Size = new System.Drawing.Size(108, 22);
            this.txt_totalCharges.TabIndex = 86;
            this.txt_totalCharges.Text = "0.00";
            // 
            // txt_amountPaid
            // 
            this.txt_amountPaid.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_amountPaid.Location = new System.Drawing.Point(124, 181);
            this.txt_amountPaid.Name = "txt_amountPaid";
            this.txt_amountPaid.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txt_amountPaid.Size = new System.Drawing.Size(108, 22);
            this.txt_amountPaid.TabIndex = 93;
            this.txt_amountPaid.Text = "0.00";
            // 
            // txt_discount
            // 
            this.txt_discount.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_discount.Location = new System.Drawing.Point(124, 33);
            this.txt_discount.Name = "txt_discount";
            this.txt_discount.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txt_discount.Size = new System.Drawing.Size(108, 22);
            this.txt_discount.TabIndex = 87;
            this.txt_discount.Text = "0.00";
            // 
            // txt_totalTax
            // 
            this.txt_totalTax.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_totalTax.Location = new System.Drawing.Point(124, 58);
            this.txt_totalTax.Name = "txt_totalTax";
            this.txt_totalTax.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txt_totalTax.Size = new System.Drawing.Size(108, 22);
            this.txt_totalTax.TabIndex = 88;
            this.txt_totalTax.Text = "0.00";
            // 
            // txt_total
            // 
            this.txt_total.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_total.Location = new System.Drawing.Point(124, 132);
            this.txt_total.Name = "txt_total";
            this.txt_total.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txt_total.Size = new System.Drawing.Size(108, 22);
            this.txt_total.TabIndex = 91;
            this.txt_total.Text = "0.00";
            // 
            // txt_deposit
            // 
            this.txt_deposit.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_deposit.Location = new System.Drawing.Point(124, 206);
            this.txt_deposit.Name = "txt_deposit";
            this.txt_deposit.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txt_deposit.Size = new System.Drawing.Size(108, 22);
            this.txt_deposit.TabIndex = 94;
            this.txt_deposit.Text = "0.00";
            // 
            // txt_extraCharges
            // 
            this.txt_extraCharges.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_extraCharges.Location = new System.Drawing.Point(124, 107);
            this.txt_extraCharges.Name = "txt_extraCharges";
            this.txt_extraCharges.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txt_extraCharges.Size = new System.Drawing.Size(108, 22);
            this.txt_extraCharges.TabIndex = 90;
            this.txt_extraCharges.Text = "0.00";
            // 
            // txt_flatDiscount
            // 
            this.txt_flatDiscount.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_flatDiscount.Location = new System.Drawing.Point(124, 156);
            this.txt_flatDiscount.Name = "txt_flatDiscount";
            this.txt_flatDiscount.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txt_flatDiscount.Size = new System.Drawing.Size(108, 22);
            this.txt_flatDiscount.TabIndex = 92;
            this.txt_flatDiscount.Text = "0.00";
            // 
            // txt_balance
            // 
            this.txt_balance.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_balance.Location = new System.Drawing.Point(124, 255);
            this.txt_balance.Name = "txt_balance";
            this.txt_balance.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txt_balance.Size = new System.Drawing.Size(108, 22);
            this.txt_balance.TabIndex = 96;
            this.txt_balance.Text = "0.00";
            // 
            // lbl_extraCharges
            // 
            this.lbl_extraCharges.AutoSize = true;
            this.lbl_extraCharges.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_extraCharges.Location = new System.Drawing.Point(8, 113);
            this.lbl_extraCharges.Name = "lbl_extraCharges";
            this.lbl_extraCharges.Size = new System.Drawing.Size(92, 16);
            this.lbl_extraCharges.TabIndex = 79;
            this.lbl_extraCharges.Text = "Extra Charges";
            // 
            // lbl_balance
            // 
            this.lbl_balance.AutoSize = true;
            this.lbl_balance.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_balance.Location = new System.Drawing.Point(8, 261);
            this.lbl_balance.Name = "lbl_balance";
            this.lbl_balance.Size = new System.Drawing.Size(59, 16);
            this.lbl_balance.TabIndex = 85;
            this.lbl_balance.Text = "Balance";
            // 
            // lbl_deposit
            // 
            this.lbl_deposit.AutoSize = true;
            this.lbl_deposit.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_deposit.Location = new System.Drawing.Point(8, 212);
            this.lbl_deposit.Name = "lbl_deposit";
            this.lbl_deposit.Size = new System.Drawing.Size(55, 16);
            this.lbl_deposit.TabIndex = 83;
            this.lbl_deposit.Text = "Deposit";
            // 
            // lbl_Total
            // 
            this.lbl_Total.AutoSize = true;
            this.lbl_Total.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Total.Location = new System.Drawing.Point(8, 138);
            this.lbl_Total.Name = "lbl_Total";
            this.lbl_Total.Size = new System.Drawing.Size(38, 16);
            this.lbl_Total.TabIndex = 80;
            this.lbl_Total.Text = "Total";
            // 
            // lbl_totalTax
            // 
            this.lbl_totalTax.AutoSize = true;
            this.lbl_totalTax.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_totalTax.Location = new System.Drawing.Point(8, 64);
            this.lbl_totalTax.Name = "lbl_totalTax";
            this.lbl_totalTax.Size = new System.Drawing.Size(63, 16);
            this.lbl_totalTax.TabIndex = 77;
            this.lbl_totalTax.Text = "Total Tax";
            // 
            // lbl_discount
            // 
            this.lbl_discount.AutoSize = true;
            this.lbl_discount.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_discount.Location = new System.Drawing.Point(8, 39);
            this.lbl_discount.Name = "lbl_discount";
            this.lbl_discount.Size = new System.Drawing.Size(61, 16);
            this.lbl_discount.TabIndex = 76;
            this.lbl_discount.Text = "Discount";
            // 
            // lbl_amountPaid
            // 
            this.lbl_amountPaid.AutoSize = true;
            this.lbl_amountPaid.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_amountPaid.Location = new System.Drawing.Point(8, 187);
            this.lbl_amountPaid.Name = "lbl_amountPaid";
            this.lbl_amountPaid.Size = new System.Drawing.Size(85, 16);
            this.lbl_amountPaid.TabIndex = 82;
            this.lbl_amountPaid.Text = "Amount Paid";
            // 
            // lbl_totalRate
            // 
            this.lbl_totalRate.AutoSize = true;
            this.lbl_totalRate.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_totalRate.Location = new System.Drawing.Point(8, 89);
            this.lbl_totalRate.Name = "lbl_totalRate";
            this.lbl_totalRate.Size = new System.Drawing.Size(69, 16);
            this.lbl_totalRate.TabIndex = 78;
            this.lbl_totalRate.Text = "Total Rate";
            // 
            // lbl_roundOff
            // 
            this.lbl_roundOff.AutoSize = true;
            this.lbl_roundOff.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_roundOff.Location = new System.Drawing.Point(8, 236);
            this.lbl_roundOff.Name = "lbl_roundOff";
            this.lbl_roundOff.Size = new System.Drawing.Size(70, 16);
            this.lbl_roundOff.TabIndex = 84;
            this.lbl_roundOff.Text = "Round Off";
            // 
            // lbl_flatDiscount
            // 
            this.lbl_flatDiscount.AutoSize = true;
            this.lbl_flatDiscount.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_flatDiscount.Location = new System.Drawing.Point(8, 162);
            this.lbl_flatDiscount.Name = "lbl_flatDiscount";
            this.lbl_flatDiscount.Size = new System.Drawing.Size(87, 16);
            this.lbl_flatDiscount.TabIndex = 81;
            this.lbl_flatDiscount.Text = "Flat Discount";
            // 
            // lbl_totalCharges
            // 
            this.lbl_totalCharges.AutoSize = true;
            this.lbl_totalCharges.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_totalCharges.Location = new System.Drawing.Point(8, 14);
            this.lbl_totalCharges.Name = "lbl_totalCharges";
            this.lbl_totalCharges.Size = new System.Drawing.Size(93, 16);
            this.lbl_totalCharges.TabIndex = 75;
            this.lbl_totalCharges.Text = "Total Charges";
            // 
            // btn_GoBack
            // 
            this.btn_GoBack.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.btn_GoBack.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btn_GoBack.FlatAppearance.BorderSize = 0;
            this.btn_GoBack.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_GoBack.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_GoBack.ForeColor = System.Drawing.Color.White;
            this.btn_GoBack.Image = global::maxxis.Properties.Resources.Go_Back;
            this.btn_GoBack.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_GoBack.Location = new System.Drawing.Point(383, 0);
            this.btn_GoBack.Name = "btn_GoBack";
            this.btn_GoBack.Size = new System.Drawing.Size(128, 37);
            this.btn_GoBack.TabIndex = 3;
            this.btn_GoBack.Text = "GO BACK";
            this.btn_GoBack.UseVisualStyleBackColor = false;
            this.btn_GoBack.Click += new System.EventHandler(this.btn_GoBack_Click);
            // 
            // RateInformation_form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Silver;
            this.ClientSize = new System.Drawing.Size(489, 324);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.pnl_rateInformation);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "RateInformation_form";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "RateInformation_form";
            this.pnl_rateInformation.ResumeLayout(false);
            this.pnl_rateInformation.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel pnl_rateInformation;
        private System.Windows.Forms.Label lbl_rateInformation;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lbl_checkIn;
        private System.Windows.Forms.Label lbl_folioNumber;
        private System.Windows.Forms.Label lbl_lastName;
        private System.Windows.Forms.Label lbl_firstName;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label lbl_checkOut;
        private System.Windows.Forms.TextBox txt_folioNumber;
        private System.Windows.Forms.TextBox txt_lastName;
        private System.Windows.Forms.TextBox txt_firstName;
        private System.Windows.Forms.DateTimePicker dtp_checkOutTime;
        private System.Windows.Forms.DateTimePicker dtp_checkOutDate;
        private System.Windows.Forms.DateTimePicker dtp_checkInTime;
        private System.Windows.Forms.DateTimePicker dtp_checkInDate;
        private System.Windows.Forms.TextBox txt_balance;
        private System.Windows.Forms.TextBox txt_flatDiscount;
        private System.Windows.Forms.TextBox txt_extraCharges;
        private System.Windows.Forms.TextBox txt_deposit;
        private System.Windows.Forms.TextBox txt_total;
        private System.Windows.Forms.TextBox txt_totalTax;
        private System.Windows.Forms.TextBox txt_discount;
        private System.Windows.Forms.TextBox txt_amountPaid;
        private System.Windows.Forms.TextBox txt_totalCharges;
        private System.Windows.Forms.TextBox txt_totalRate;
        private System.Windows.Forms.TextBox txt_roundOff;
        private System.Windows.Forms.Label lbl_flatDiscount;
        private System.Windows.Forms.Label lbl_roundOff;
        private System.Windows.Forms.Label lbl_totalRate;
        private System.Windows.Forms.Label lbl_amountPaid;
        private System.Windows.Forms.Label lbl_discount;
        private System.Windows.Forms.Label lbl_totalTax;
        private System.Windows.Forms.Label lbl_Total;
        private System.Windows.Forms.Label lbl_deposit;
        private System.Windows.Forms.Label lbl_balance;
        private System.Windows.Forms.Label lbl_extraCharges;
        private System.Windows.Forms.Label lbl_totalCharges;
        private System.Windows.Forms.Button btn_GoBack;
    }
}